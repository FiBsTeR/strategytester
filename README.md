# StrategyTester
This is a framework for evaluating the performance of strategies for board games and card games.

Currently, the following games are implemented:

* Tic-tac-toe
* Rock, paper, scissors
* [Hanabi](https://en.wikipedia.org/wiki/Hanabi_(card_game))

## Quickstart
Here's an example invocation for Tic-tac-toe:
```shell
strategytester/ $ mkdir bin
strategytester/ $ javac -d bin -sourcepath src/**/*.java
strategytester/ $ java -cp bin tictactoe.Main
===== Random vs Random (10000 total games) =====
[...]
First player win: 5798 games
Draw: 1310 games
Second player win: 2892 games
[...]
===== Random vs Optimal (10000 total games) =====
Draw: 1405 games
Second player win: 8595 games
[...]
```

The first part of the output shows the result of testing a Tic-tac-toe strategy that randomly chooses moves against itself over 10k games. The second part of the output shows the result of testing that same random strategy against an optimal strategy over 10k games. The output also contains sample game histories (i.e. the moves played by each side during a single game) for each outcome.

## Implementing other games
To implement a new game, you need to define a few components about the game by implementing the following interfaces in the `strategytester` package:

* `Action`: Define all of the decisions a player can make on their turn. Eg for Rock, paper, scissors, the actions are choosing "Rock", "Paper", or "Scissors". For Tic-tac-toe, the actions are choosing one of the 9 squares in the grid.
* `GameState`: Define what a single state of the game is and what actions are legal from that state. Also, define how to compute the resulting game state when applying an action on a given state. Finally, define which game states are "terminal", in the sense that the game is over when that state is reached.
* `InitialGameStateProvider`: Define how to compute the game state for a new game. For some games like Tic-tac-toe, this can return the same game state every time (e.g. an empty board). But for some games, there may be some randomness involved, such as computing the initial sorting of a deck of cards.
* `Strategy`: Define a strategy that chooses an action to make from a given game state.

Once these are implemented, you can define a main method to run strategies against each other using the `StrategyTester` class. See the Tic-tac-toe and Rock, paper, scissors implementations for inspiration.
