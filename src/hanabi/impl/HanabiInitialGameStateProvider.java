package hanabi.impl;

import java.util.ArrayList;
import java.util.List;

import hanabi.common.HanabiConfig;
import strategytester.InitialGameStateProvider;

final class HanabiInitialGameStateProvider implements InitialGameStateProvider<HanabiGameState, HanabiAction>{

  private final HanabiConfig config;
  private long rngSeed;
  
  HanabiInitialGameStateProvider(HanabiConfig config) {
    this.config = config;
    rngSeed = 0;
  }
  
  @Override
  public InitialGameState<HanabiGameState> provideRandom() {
    InitialGameState<HanabiGameState> ans
        = new InitialGameState<>(rngSeed /* gameId */, provideForGameId(rngSeed));
    rngSeed++;
    return ans;
        
  }
  
  @Override
  public HanabiGameState provideForGameId(long gameId) {
    DealableDeck deck = DealableDeck.fromSpecAndShuffled(config.getDeckSpecification(), gameId);
    List<HanabiHand> hands = new ArrayList<HanabiHand>();
    for (int i = 0; i < config.getNumPlayers(); i++) {
      HanabiHand hand = HanabiHand.EMPTY;
      for (int j = 0; j < config.getInitialCardsInHand(); j++) {
        DealableDeck.DealResult result = deck.deal();
        deck = result.getNewDeck();
        hand = hand.addCard(result.getDealtCardValue());
      }
      hands.add(hand);
    }
    return new HanabiGameState(
        config, 
        deck,
        AppendableTrash.EMPTY,
        AppendableCommunity.EMPTY,
        hands,
        0 /* playerToAct */,
        config.getInitialNumHints(),
        config.getInitialNumFuses(),
        config.getMovesPerPlayerAfterEmptyDeck());
  }
}
