package hanabi.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import hanabi.common.Conventions;
import hanabi.common.HanabiConfig;
import hanabi.common.Move;
import hanabi.common.Move.Clue;
import hanabi.common.Move.Discard;
import hanabi.common.Move.Play;
import hanabi.common.RevealedHand;
import strategytester.Strategy;

final class ConventionsStrategy implements Strategy<HanabiGameState, HanabiAction> {

  private final Conventions conventions;
  
  public ConventionsStrategy(Conventions conventions) {
    this.conventions = conventions;
  }
  
  @Override
  public String getName() {
    return conventions.getName();
  }

  @Override
  public HanabiAction act(HanabiGameState gameState) {
    List<RevealedHand> hands = gameState.getHands();
    List<Optional<RevealedHand>> otherHands = new ArrayList<Optional<RevealedHand>>();
    int playerToAct = gameState.getPlayerToAct();
    // Hide the player's hand from them.
    for (int i = 0; i < hands.size(); i++) {
      Optional<RevealedHand> otherHand = i == playerToAct
          ? Optional.empty()
          : Optional.of(hands.get(i));
      otherHands.add(otherHand);
    }

    Move move = conventions.makeMove(
        gameState.getConfig(), 
        gameState.getDeck(), 
        gameState.getTrash(), 
        gameState.getCommunity(), 
        otherHands, 
        hands.get(playerToAct).asHintedHand(), 
        playerToAct, 
        gameState.getHintsRemaining(), 
        gameState.getFusesRemaining(), 
        gameState.getMovesAfterEmptyDeck());
    
    if (move instanceof Clue) {
      Clue clueMove = (Clue) move;
      return new ClueAction(clueMove.getPlayer(), clueMove.getHint());
    } else if (move instanceof Discard) {
      return new DiscardAction(((Discard) move).getIndex());
    } else if (move instanceof Play) {
      return new PlayAction(((Play) move).getIndex());
    } else {
      throw new RuntimeException("Unexpected move.");
    }
  }
}
