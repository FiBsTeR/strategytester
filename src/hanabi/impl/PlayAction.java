package hanabi.impl;

final class PlayAction extends HanabiAction {

  private final int index;
  
  PlayAction(int index) {
    this.index = index;
  }
  
  @Override
  protected String describeInternal() {
    return String.format("Played card at index %s", index);
  }

  @Override
  protected boolean isLegalOn(HanabiGameState gameState) {
    return gameState.isPlayLegal(index);
  }

  @Override
  protected HanabiGameState applyTo(HanabiGameState gameState) {
    return gameState.applyPlay(index);
  }
}
