package hanabi.impl;

import java.util.Comparator;
import java.util.HashMap;
import java.util.Map;
import java.util.stream.Collectors;

import hanabi.common.CardValue;
import hanabi.common.Trash;

final class AppendableTrash implements Trash {

  private final Map<CardValue, Integer> counts;
  
  private AppendableTrash(Map<CardValue, Integer> trashedCounts) {
    this.counts = trashedCounts;
  }
  
  static AppendableTrash EMPTY = new AppendableTrash(new HashMap<CardValue, Integer>());
  
  @Override
  public int countOf(CardValue cardValue) {
    return counts.getOrDefault(cardValue, 0);
  }
  
  @Override
  public Map<CardValue, Integer> asMap() {
    return new HashMap<CardValue, Integer>(counts);
  }

  AppendableTrash add(CardValue cardValue) {
    Map<CardValue, Integer> newTrashedCounts = new HashMap<CardValue, Integer>(counts);
    newTrashedCounts.compute(cardValue, (key, count) -> count == null ? 1 : count + 1);
    return new AppendableTrash(newTrashedCounts);
  }
  
  String describe() {
    return counts.entrySet()
        .stream()
        .sorted(Comparator.comparing(e -> e.getKey(), CardValue.COMPARATOR))
        .map(e -> e.getValue() == 1 ? e.getKey().describe() : String.format("%s(x%s)", e.getKey().describe(), e.getValue()))
        .collect(Collectors.joining(" "));
  }
}
