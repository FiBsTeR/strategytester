package hanabi.impl;

import strategytester.Outcome;

final class HanabiOutcome implements Outcome {

  private final int numPoints;
  
  HanabiOutcome(int numPoints) {
    this.numPoints = numPoints;
  }
  
  @Override
  public int getValue() {
    return numPoints;
  }

  @Override
  public String describe() {
    return numPoints == 1
        ? "1 point"
        : String.format("%s points", numPoints);
  }
}
