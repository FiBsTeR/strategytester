package hanabi.impl;

import java.util.ArrayList;
import java.util.List;

import hanabi.common.CardValue;
import hanabi.common.Community;
import hanabi.common.Deck;
import hanabi.common.HanabiConfig;
import hanabi.common.Hint;
import hanabi.common.RevealedHand;
import hanabi.common.Trash;
import strategytester.GameState;
import strategytester.Outcome;

final class HanabiGameState implements GameState<HanabiGameState, HanabiAction> {

  private final HanabiConfig config;
  private final DealableDeck deck;
  private final AppendableTrash trash;
  private final AppendableCommunity community;
  private final List<HanabiHand> hands;
  private final int playerToAct;
  private final int hintsRemaining;
  private final int fusesRemaining;
  private final int movesAfterEmptyDeck;
  
  HanabiGameState(
      HanabiConfig config, 
      DealableDeck deck, 
      AppendableTrash trash,
      AppendableCommunity community,
      List<HanabiHand> hands,
      int playerToAct,
      int hintsRemaining,
      int fusesRemaining,
      int movesAfterEmptyDeck) {
    this.config = config;
    this.deck = deck;
    this.trash = trash;
    this.community = community;
    this.hands = hands;
    this.playerToAct = playerToAct;
    this.hintsRemaining = hintsRemaining;
    this.fusesRemaining = fusesRemaining;
    this.movesAfterEmptyDeck = movesAfterEmptyDeck;
  }
  
  @Override
  public boolean isTerminal() {
    return community.getNumCardsPlayed() >= config.getMaxPlayableCards()
        || fusesRemaining <= 0
        || (deck.getNumCardsRemaining() <= 0 &&
            movesAfterEmptyDeck >= config.getNumPlayers() * config.getMovesPerPlayerAfterEmptyDeck());
  }

  @Override
  public int getPlayerToAct() {
    return playerToAct;
  }

  @Override
  public boolean isActionLegal(HanabiAction action) {
    return action.isLegalOn(this);
  }

  @Override
  public HanabiGameState applyAction(HanabiAction action) {
    return action.applyTo(this);
  }

  @Override
  public Outcome getOutcome() {
    if (!isTerminal()) {
      throw new RuntimeException("Attempted to get outcome for in-progress game.");
    }
    return new HanabiOutcome(community.getNumCardsPlayed());
  }

  @Override
  public String describe() {
    // TODO(jven): Describe hints.
    String s = String.format(
        "Deck = %s\nCommunity = %s\nTrash = %s", 
        deck.describe(), 
        community.describe(), 
        trash.describe());
    for (int i = 0; i < config.getNumPlayers(); i++) {
      s += String.format("\nPlayer %s hand = %s", i, hands.get(i).describe());
    }
    s += "\n";
    return s;
  }
  
  // Package-private accessors
  
  HanabiConfig getConfig() {
    return config;
  }
  
  Deck getDeck() {
    return deck;
  }
  
  Trash getTrash() {
    return trash;
  }
  
  Community getCommunity() {
    return community;
  }
  
  List<RevealedHand> getHands() {
    return new ArrayList<RevealedHand>(hands);
  }
  
  int getHintsRemaining() {
    return hintsRemaining;
  }
  
  int getFusesRemaining() {
    return fusesRemaining;
  }
  
  int getMovesAfterEmptyDeck() {
    return movesAfterEmptyDeck;
  }
  
  // Package-private action methods
  
  boolean isClueLegal(int player, Hint hint) {
    if (!isPlayerInBounds(player) || isPlayerToAct(player)) {
      // You can't clue yourself.
      return false;
    }
    if (hintsRemaining <= 0) {
      // No more hints.
      return false;
    }

    // The clue must apply to at least one card in the player's hand.
    return hands.get(player).doesHintApplyToAnyCard(hint);
  }
  
  HanabiGameState applyClue(int player, Hint hint) {
    if (!isClueLegal(player, hint)) {
      throw new RuntimeException("Clue is not legal.");
    }

    List<HanabiHand> newHands = new ArrayList<HanabiHand>(hands);
    newHands.set(player, newHands.get(player).applyHint(hint));
    
    return new HanabiGameState(
        config,
        deck,
        trash,
        community,
        newHands,
        nextPlayerToAct(),
        // Hinting consumes a hint.
        hintsRemaining - 1,
        fusesRemaining,
        nextNumMovesAfterEmptyDeck());
  }
  
  boolean isDiscardLegal(int index) {
    // You can't discard if you have the maximum number of hints.
    return hintsRemaining < config.getMaxNumHints();
  }
  
  HanabiGameState applyDiscard(int index) {
    if (!isDiscardLegal(index)) {
      throw new RuntimeException("Discard is not legal.");
    }

    List<HanabiHand> newHands = new ArrayList<HanabiHand>(hands);
    HanabiHand newHand = newHands.get(playerToAct);
    CardValue cardToDiscard = newHand.getCardValueAt(index);
    newHand = newHand.removeCardAt(index);
    DealableDeck newDeck;
    if (deck.getNumCardsRemaining() > 0) {
      DealableDeck.DealResult result = deck.deal();
      newDeck = result.getNewDeck();
      newHand = newHand.addCard(result.getDealtCardValue());
    } else {
      newDeck = deck;
    }
    newHands.set(playerToAct, newHand);

    return new HanabiGameState(
        config,
        newDeck,
        trash.add(cardToDiscard),
        community,
        newHands,
        nextPlayerToAct(),
        // Discarding gives an additional hint.
        hintsRemaining + 1,
        fusesRemaining,
        nextNumMovesAfterEmptyDeck());
  }
  
  boolean isPlayLegal(int index) {
    // You can always play a card.
    return true;
  }
  
  HanabiGameState applyPlay(int index) {
    if (!isPlayLegal(index)) {
      throw new RuntimeException("Play is not legal.");
    }

    List<HanabiHand> newHands = new ArrayList<HanabiHand>(hands);
    HanabiHand newHand = newHands.get(playerToAct);
    CardValue cardToPlay = newHand.getCardValueAt(index);
    newHand = newHand.removeCardAt(index);
    DealableDeck newDeck;
    if (deck.getNumCardsRemaining() > 0) {
      DealableDeck.DealResult result = deck.deal();
      newDeck = result.getNewDeck();
      newHand = newHand.addCard(result.getDealtCardValue());
    } else {
      newDeck = deck;
    }
    newHands.set(playerToAct, newHand);
    
    if (community.isPlayable(cardToPlay)) {
      return new HanabiGameState(
          config,
          newDeck,
          trash,
          community.add(cardToPlay),
          newHands,
          nextPlayerToAct(),
          hintsRemaining,
          fusesRemaining,
          nextNumMovesAfterEmptyDeck());
    }
    
    return new HanabiGameState(
        config,
        newDeck,
        // The card that failed to be played is trashed.
        trash.add(cardToPlay),
        community,
        newHands,
        nextPlayerToAct(),
        hintsRemaining,
        // A fuse is consumed by the failed play.
        fusesRemaining - 1,
        nextNumMovesAfterEmptyDeck());
  }
  
  // Private methods
  
  private boolean isPlayerInBounds(int player) {
    return player >= 0 && player < config.getNumPlayers();
  }
  
  private boolean isPlayerToAct(int player) {
    return player == playerToAct;
  }
  
  private int nextPlayerToAct() {
    return (playerToAct + 1) % config.getNumPlayers();
  }
  
  private int nextNumMovesAfterEmptyDeck() {
    return deck.getNumCardsRemaining() == 0
        ? movesAfterEmptyDeck + 1
        : 0;
  }
}
