package hanabi.impl;

import hanabi.common.Hint;

final class ClueAction extends HanabiAction {

  private final int player;
  private final Hint hint;
  
  ClueAction(int player, Hint hint) {
    this.player = player;
    this.hint = hint;
  }
  
  @Override
  protected String describeInternal() {
    return String.format("Clued player %s about '%s'", player, hint.describe());
  }

  @Override
  protected boolean isLegalOn(HanabiGameState gameState) {
    return gameState.isClueLegal(player, hint);
  }

  @Override
  protected HanabiGameState applyTo(HanabiGameState gameState) {
    return gameState.applyClue(player, hint);
  }
}
