package hanabi.impl;

import java.util.HashMap;
import java.util.Map;
import java.util.stream.Collectors;

import hanabi.common.CardValue;
import hanabi.common.Color;
import hanabi.common.Community;

final class AppendableCommunity implements Community {

  private final Map<Color, Integer> stacks;
  
  AppendableCommunity(Map<Color, Integer> stacks) {
    this.stacks = stacks;
  }
  
  static AppendableCommunity EMPTY = new AppendableCommunity(new HashMap<Color, Integer>());
  
  @Override
  public int getHighestPlayedNumberForColor(Color color) {
    return stacks.getOrDefault(color, 0);
  }
  
  @Override
  public int getNumCardsPlayed() {
    return stacks.values().stream().mapToInt(i -> i).sum();
  }
  
  @Override
  public boolean isPlayable(CardValue cardValue) {
    return cardValue.getNumber() == 1 + getHighestPlayedNumberForColor(cardValue.getColor());
  }

  AppendableCommunity add(CardValue cardValue) {
    if (!isPlayable(cardValue)) {
      throw new RuntimeException("Failed to play card.");
    }
    Map<Color, Integer> newStacks = new HashMap<Color, Integer>(stacks);
    newStacks.compute(cardValue.getColor(), (key, value) -> value == null ? 1 : value + 1);
    return new AppendableCommunity(newStacks);
  }
  
  String describe() {
    return stacks.entrySet()
        .stream()
        .map(e -> new CardValue(e.getValue(), e.getKey()).describe())
        .collect(Collectors.joining(" "));
  }
}
