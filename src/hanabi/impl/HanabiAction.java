package hanabi.impl;

import strategytester.Action;

abstract class HanabiAction implements Action {
  
  @Override
  public final String describe() {
    return describeInternal();
  }
  
  protected abstract String describeInternal();
  
  protected abstract boolean isLegalOn(HanabiGameState gameState);
  
  protected abstract HanabiGameState applyTo(HanabiGameState gameState);
}
