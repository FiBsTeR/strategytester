package hanabi.impl;

final class DiscardAction extends HanabiAction {

  private final int index;
  
  DiscardAction(int index) {
    this.index = index;
  }
  
  @Override
  protected String describeInternal() {
    return String.format("Discarded card at index %s", index);
  }

  @Override
  protected boolean isLegalOn(HanabiGameState gameState) {
    return gameState.isDiscardLegal(index);
  }

  @Override
  protected HanabiGameState applyTo(HanabiGameState gameState) {
    return gameState.applyDiscard(index);
  }
}
