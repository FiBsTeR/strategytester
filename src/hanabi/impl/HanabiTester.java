package hanabi.impl;

import java.util.ArrayList;
import java.util.List;

import hanabi.common.Conventions;
import hanabi.common.HanabiConfig;
import strategytester.GamePlayer;
import strategytester.Strategy;
import strategytester.StrategyTester;
import strategytester.StrategyTester.Result;

/**
 * Contains a static utility to test Hanabi conventions with the given game variant.
 * This is the only public API of the impl package and is used by the Main script.
 * 
 * Conventions are implemented in the hanabi.conventions package.
 * All other public APIs are in the hanabi.common package.
 */
public final class HanabiTester {
  
  private HanabiTester() {}
  
  public static StrategyTester<HanabiGameState, HanabiAction>.Result testConventions(
      Conventions.Factory conventionsFactory, HanabiConfig config, int numGames) {
    StrategyTester<HanabiGameState, HanabiAction> strategyTester 
        = new StrategyTester<HanabiGameState, HanabiAction>(
              new HanabiInitialGameStateProvider(config), new GamePlayer<HanabiGameState, HanabiAction>());
    List<Strategy<HanabiGameState, HanabiAction>> strategies 
        = new ArrayList<Strategy<HanabiGameState, HanabiAction>>();
    strategies.add(new ConventionsStrategy(conventionsFactory.create()));
    strategies.add(new ConventionsStrategy(conventionsFactory.create()));
    return strategyTester.testStrategies(strategies, numGames);
  }
  
  public static StrategyTester<HanabiGameState, HanabiAction>.Result testConventionsForGameId(
      Conventions.Factory conventionsFactory, HanabiConfig config, long gameId) {
    StrategyTester<HanabiGameState, HanabiAction> strategyTester 
        = new StrategyTester<HanabiGameState, HanabiAction>(
              new HanabiInitialGameStateProvider(config), new GamePlayer<HanabiGameState, HanabiAction>());
    List<Strategy<HanabiGameState, HanabiAction>> strategies 
        = new ArrayList<Strategy<HanabiGameState, HanabiAction>>();
    strategies.add(new ConventionsStrategy(conventionsFactory.create()));
    strategies.add(new ConventionsStrategy(conventionsFactory.create()));
    return strategyTester.testStrategiesForGameId(strategies, gameId);
  }
}
