package hanabi.impl;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import hanabi.common.CardValue;
import hanabi.common.Hint;
import hanabi.common.HintedCard;
import hanabi.common.HintedHand;
import hanabi.common.PredicatedHint;
import hanabi.common.RevealedHand;

final class HanabiHand implements RevealedHand, HintedHand {

  // The 0th indexed card is the newest card. The cards are sorted by age.
  private final List<HanabiCard> cards;
  
  // The 0th indexed hint is the oldest hint. The cards are sorted in descending order of age.
  private final List<Hint> hints;
  
  // predicatedHints[i][j] is the predicated hint for card i and hint j, or an empty optional if the card was not
  // present for that hint.
  private final List<List<Optional<PredicatedHint>>> predicatedHints;
  
  private HanabiHand(List<HanabiCard> cards, List<Hint> hints, List<List<Optional<PredicatedHint>>> predicatedHints) {
    this.cards = cards;
    this.hints = hints;
    this.predicatedHints = predicatedHints;
  }
  
  static HanabiHand EMPTY = new HanabiHand(new ArrayList<>(), new ArrayList<>(), new ArrayList<>());
  
  @Override
  public int size() {
    return cards.size();
  }
  
  @Override
  public HintedCard getHintedCardAt(int cardIndex) {
    checkCardIndex(cardIndex);
    return cards.get(cardIndex);
  }
  
  @Override
  public int getNumHints() {
    return hints.size();
  }
  
  @Override
  public Hint getHint(int hintIndex) {
    checkHintIndex(hintIndex);
    return hints.get(hintIndex);
  }
  
  @Override
  public Optional<PredicatedHint> getPredicatedHintForCardAt(int hintIndex, int cardIndex) {
    checkHintIndex(hintIndex);
    checkCardIndex(cardIndex);
    return predicatedHints.get(cardIndex).get(hintIndex);
  }

  @Override
  public CardValue getCardValueAt(int cardIndex) {
    checkCardIndex(cardIndex);
    return cards.get(cardIndex).getCardValue();
  }

  @Override
  public HintedHand asHintedHand() {
    return this;
  }
  
  HanabiHand removeCardAt(int cardIndex) {
    checkCardIndex(cardIndex);
    LinkedList<HanabiCard> newCards = new LinkedList<>(cards);
    newCards.remove(cardIndex);
    LinkedList<List<Optional<PredicatedHint>>> newPredicatedHints = new LinkedList<>(predicatedHints);
    newPredicatedHints.remove(cardIndex);
    return new HanabiHand(newCards, hints, newPredicatedHints);
  }
  
  HanabiHand addCard(CardValue cardValue) {
    LinkedList<HanabiCard> newCards = new LinkedList<>(cards);
    // The newest card must be added to the start of the new list.
    newCards.addFirst(HanabiCard.createWithNoHints(cardValue));
    LinkedList<List<Optional<PredicatedHint>>> newPredicatedHints = new LinkedList<>(predicatedHints);
    ArrayList<Optional<PredicatedHint>> newPredicatedHintsForNewCard = new ArrayList<>();
    for (int i = 0; i < hints.size(); i++) {
      newPredicatedHintsForNewCard.add(Optional.empty());
    }
    newPredicatedHints.addFirst(newPredicatedHintsForNewCard);
    return new HanabiHand(newCards, hints, newPredicatedHints);
  }

  HanabiHand applyHint(Hint hint) {
    LinkedList<HanabiCard> newCards = new LinkedList<HanabiCard>();
    for (HanabiCard card : cards) {
      // The cards are visited in increasing order of age. Each subsequent card should be added to the end of the new
      // list.
      newCards.addLast(card.addHint(hint));
    }
    ArrayList<Hint> newHints = new ArrayList<>(hints);
    newHints.add(hint);
    ArrayList<List<Optional<PredicatedHint>>> newPredicatedHints = new ArrayList<>(predicatedHints);
    for (int i = 0; i < newPredicatedHints.size(); i++) {
      ArrayList<Optional<PredicatedHint>> newPredicatedHintsForCard = new ArrayList<>(newPredicatedHints.get(i));
      newPredicatedHintsForCard.add(Optional.of(hint.applyTo(cards.get(i).getCardValue())));
      newPredicatedHints.set(i, newPredicatedHintsForCard);
    }
    
    return new HanabiHand(newCards, newHints, newPredicatedHints);
  }
  
  boolean doesHintApplyToAnyCard(Hint hint) {
    return cards.stream()
        .anyMatch(c -> hint.applyTo(c.getCardValue()).didApply());
  }
  
  String describe() {
    return cards.stream()
        .map(HanabiCard::getCardValue)
        .map(CardValue::describe)
        .collect(Collectors.joining(" "));
  }
  
  private void checkCardIndex(int cardIndex) {
    if (cardIndex < 0 || cardIndex >= cards.size()) {
      throw new RuntimeException(String.format("Invalid card index: %s", cardIndex));
    }
  }
  
  private void checkHintIndex(int hintIndex) {
    if (hintIndex < 0 || hintIndex >= hints.size()) {
      throw new RuntimeException(String.format("Invalid hint index: %s", hintIndex));
    }
  }
}
