package hanabi.impl;

import java.util.ArrayList;
import java.util.List;

import hanabi.common.CardValue;
import hanabi.common.Hint;
import hanabi.common.HintedCard;
import hanabi.common.PredicatedHint;

final class HanabiCard implements HintedCard {

  private final CardValue cardValue;
  private final List<PredicatedHint> predicatedHints;
  
  private HanabiCard(CardValue cardValue, List<PredicatedHint> predicatedHints) {
    this.cardValue = cardValue;
    this.predicatedHints = predicatedHints;
  }
  
  static HanabiCard createWithNoHints(CardValue cardValue) {
    return new HanabiCard(cardValue, new ArrayList<>());
  }

  @Override
  public List<PredicatedHint> getPredicatedHints() {
    return new ArrayList<PredicatedHint>(predicatedHints);
  }
  
  CardValue getCardValue() {
    return cardValue;
  }
  
  HanabiCard addHint(Hint hint) {
    List<PredicatedHint> newPredicatedHints = new ArrayList<PredicatedHint>(predicatedHints);
    newPredicatedHints.add(hint.applyTo(cardValue));
    return new HanabiCard(cardValue, newPredicatedHints);
  }
}
