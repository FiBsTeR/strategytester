package hanabi.impl;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Random;
import java.util.stream.Collectors;

import hanabi.common.CardValue;
import hanabi.common.Deck;
import hanabi.common.DeckSpecification;

final class DealableDeck implements Deck {

  private final List<CardValue> cardValues;
  private final int nextToDeal;
  
  private DealableDeck(List<CardValue> cardValues, int nextToDeal) {
    this.cardValues = cardValues;
    this.nextToDeal = nextToDeal;
  }
  
  static DealableDeck fromSpecAndShuffled(DeckSpecification spec, long rngSeed) {
    List<CardValue> cardValues = new ArrayList<CardValue>();
    spec
        .asMap()
        .entrySet()
        .stream()
        .forEach(entry -> {
          for (int i = 0; i < entry.getValue(); i++) {
            cardValues.add(entry.getKey());
          }
        });
    Collections.shuffle(cardValues, new Random(rngSeed));
    return new DealableDeck(cardValues, 0 /* nextToDeal */);
  }
  
  @Override
  public int getNumCardsRemaining() {
    return cardValues.size() - nextToDeal;
  }
  
  DealResult deal() {
    if (nextToDeal >= cardValues.size()) {
      throw new RuntimeException("Deck is empty.");
    }
    return new DealResult(
        new DealableDeck(cardValues, nextToDeal + 1), 
        cardValues.get(nextToDeal));
  }
  
  String describe() {
    return cardValues
        .subList(nextToDeal, cardValues.size())
        .stream()
        .map(CardValue::describe)
        .collect(Collectors.joining(" "));
  }

  static final class DealResult {
    private final DealableDeck newDeck;
    private final CardValue dealtCardValue;
    
    private DealResult(DealableDeck newDeck, CardValue dealtCardValue) {
      this.newDeck = newDeck;
      this.dealtCardValue = dealtCardValue;
    }
    
    public DealableDeck getNewDeck() {
      return newDeck;
    }
    
    public CardValue getDealtCardValue() {
      return dealtCardValue;
    }
  }
}
