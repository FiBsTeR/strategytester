package hanabi.variants;

import hanabi.common.Color;
import hanabi.common.HanabiConfig;

public final class Variants {

  private Variants() {}
  
  public static final HanabiConfig TUTORIAL = HanabiConfig.forDeckSpecification(
      MapDeckSpecification
          .newBuilder()
          .addCards(1, Color.RED, 5)
          .addCards(2, Color.RED, 5)
          .addCards(3, Color.RED, 5)
          .build())
      .numPlayers(2)
      .movesPerPlayerAfterEmptyDeck(5)
      .initialNumHints(20)
      .maxNumHints(50)
      .initialNumFuses(1)
      .maxPlayableCards(3)
      .build();
  
  public static final HanabiConfig NORMAL = HanabiConfig.forDeckSpecification(
      MapDeckSpecification
          .newBuilder()
          .addCards(1, new Color[] {Color.RED, Color.BLUE, Color.GREEN, Color.PURPLE, Color.YELLOW}, 3)
          .addCards(2, new Color[] {Color.RED, Color.BLUE, Color.GREEN, Color.PURPLE, Color.YELLOW}, 2)
          .addCards(3, new Color[] {Color.RED, Color.BLUE, Color.GREEN, Color.PURPLE, Color.YELLOW}, 2)
          .addCards(4, new Color[] {Color.RED, Color.BLUE, Color.GREEN, Color.PURPLE, Color.YELLOW}, 2)
          .addCards(5, new Color[] {Color.RED, Color.BLUE, Color.GREEN, Color.PURPLE, Color.YELLOW}, 1)
          .build())
      .numPlayers(2)
      .movesPerPlayerAfterEmptyDeck(1)
      .initialNumHints(8)
      .maxNumHints(8)
      .initialNumFuses(3)
      .maxPlayableCards(25)
      .build();
}
