package hanabi.variants;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import hanabi.common.CardValue;
import hanabi.common.Color;
import hanabi.common.DeckSpecification;

final class MapDeckSpecification implements DeckSpecification {
  
  private final Map<CardValue, Integer> counts;
  private final Set<Color> colorsUsed;
  private final Map<Color, Integer> highestNumbers;
  
  private MapDeckSpecification(Builder builder) {
    counts = builder.counts;
    colorsUsed = new HashSet<Color>();
    highestNumbers = new HashMap<Color, Integer>();
    counts.entrySet().stream().forEach(entry -> {
      Color color = entry.getKey().getColor();
      colorsUsed.add(color);
      highestNumbers.compute(color, (key, currentHighest) -> 
          currentHighest == null 
              ? entry.getValue() 
              : Math.max(currentHighest, entry.getValue()));
    });
  }
  
  public static Builder newBuilder() {
    return new Builder();
  }

  @Override
  public Set<Color> getColorsUsed() {
    return new HashSet<Color>(colorsUsed);
  }

  @Override
  public int getHighestNumberFor(Color color) {
    if (!highestNumbers.containsKey(color)) {
      throw new RuntimeException(String.format("The deck did not have any cards of color %s.", color));
    }
    return highestNumbers.get(color);
  }
  
  @Override
  public int countOf(CardValue cardValue) {
    return counts.getOrDefault(cardValue, 0);
  }
  
  @Override
  public Map<CardValue, Integer> asMap() {
    return new HashMap<CardValue, Integer>(counts);
  }
  
  static final class Builder {
    private final Map<CardValue, Integer> counts;
    
    private Builder() {
      counts = new HashMap<CardValue, Integer>();
    }
    
    Builder addCards(int number, Color color, int count) {
      return addCards(new CardValue(number, color), count);
    }
    
    Builder addCards(int number, Color[] colors, int count) {
      for (Color color : colors) {
        addCards(number, color, count);
      }
      return this;
    }
    
    Builder addCards(CardValue cardValue, int count) {
      counts.compute(cardValue, (key, value) -> value == null ? count : value + count);
      return this;
    }
    
    MapDeckSpecification build() {
      return new MapDeckSpecification(this);
    }
  }
}
