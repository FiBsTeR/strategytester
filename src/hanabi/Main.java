package hanabi;

import hanabi.conventions.examplefuncs.ExampleFuncsConventions;
import hanabi.conventions.fibbybot.FibbyBotConventions;
import hanabi.conventions.optimisticbot.OptimisticConventions;
import hanabi.impl.HanabiTester;
import hanabi.variants.Variants;

public final class Main {

  private Main() {}
  
  public static void main(String[] args) {
//    HanabiTester.testConventions(new OptimisticConventions(), Variants.TUTORIAL, 10000 /* numGames */);
//    HanabiTester.testConventions(new ExampleFuncsConventions(), Variants.TUTORIAL, 10000 /* numGames */);
//   
//    HanabiTester.testConventions(new OptimisticConventions(), Variants.NORMAL, 10000 /* numGames */);
//    HanabiTester.testConventions(new ExampleFuncsConventions(), Variants.NORMAL, 10000 /* numGames */);
    HanabiTester.testConventions(
        () -> new FibbyBotConventions(), Variants.NORMAL, 10000 /* numGames */)
        .printSummary()
        .printStatistics()
        .printHistogram()
        .printSampleHistoryForOutcome(0);
  }
}
