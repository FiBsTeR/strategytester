package hanabi.conventions.optimisticbot;

import java.util.List;
import java.util.Optional;

import hanabi.common.Community;
import hanabi.common.Conventions;
import hanabi.common.Deck;
import hanabi.common.HanabiConfig;
import hanabi.common.HintedHand;
import hanabi.common.Move;
import hanabi.common.RevealedHand;
import hanabi.common.Trash;

public final class OptimisticConventions implements Conventions {

  @Override
  public String getName() {
    return "Optimistic";
  }

  @Override
  public Move makeMove(
      HanabiConfig config,
      Deck deck,
      Trash trash,
      Community community,
      List<Optional<RevealedHand>> otherHands,
      HintedHand myHand,
      int myPlayerIndex,
      int hints,
      int fuses,
      int movesAfterEmptyDeck) {
    return Move.play(0);
  }
}
