package hanabi.conventions.fibbybot;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.stream.IntStream;

import hanabi.common.CardValue;
import hanabi.common.Color;
import hanabi.common.Community;
import hanabi.common.Conventions;
import hanabi.common.Deck;
import hanabi.common.HanabiConfig;
import hanabi.common.Hint;
import hanabi.common.HintedCard;
import hanabi.common.HintedHand;
import hanabi.common.Move;
import hanabi.common.PredicatedHint;
import hanabi.common.RevealedHand;
import hanabi.common.Trash;

public final class FibbyBotConventions implements Conventions {

  private int lastNumHints = 0;
  
  @Override
  public String getName() {
    return "FibbyBot";
  }

  @Override
  public Move makeMove(
      HanabiConfig config, 
      Deck deck, 
      Trash trash, 
      Community community,
      List<Optional<RevealedHand>> otherHands,
      HintedHand myHand,
      int myPlayerIndex, 
      int hints, 
      int fuses,
      int movesAfterEmptyDeck) {
    List<Optional<Move>> moves = new ArrayList<>();
    moves.add(tryToHintPlay(otherHands, community, hints));
    moves.add(tryToPlay(myHand, community));
    moves.add(tryToHintSave(config, otherHands, community, trash, hints));
    moves.add(tryToDiscard(config, myHand, hints));
    lastNumHints = myHand.getNumHints();
    
    return moves.stream()
        .filter(Optional::isPresent)
        .map(Optional::get)
        .findFirst()
        .orElse(Move.play(0));
  }
  
  private Optional<Move> tryToHintSave(
      HanabiConfig config,
      List<Optional<RevealedHand>> otherHands, 
      Community community, 
      Trash trash, 
      int hints) {
    if (hints <= 0) {
      return Optional.empty();
    }
    for (int playerIndex = 0; playerIndex < otherHands.size(); playerIndex++) {
      if (!otherHands.get(playerIndex).isPresent()) {
        continue;
      }
      RevealedHand hand = otherHands.get(playerIndex).get();
      for (int cardIndex = hand.size() - 1; cardIndex >= 0; cardIndex--) {
        if (isCardHinted(hand.asHintedHand(), cardIndex)) {
          continue;
        }
        CardValue card = hand.getCardValueAt(cardIndex);
        // If we're at the max number of hints, just signal a save, even if it isn't necessary.
        if (hints < config.getMaxNumHints()) {
          if (community.getHighestPlayedNumberForColor(card.getColor()) >= card.getNumber()) {
            continue;
          }
          if (trash.countOf(card) < config.getDeckSpecification().countOf(card) - 1) {
            continue;
          }
        }
        // This is the player's oldest unhinted card and it must be saved.
        // TODO(jven): Be smart about hinting number vs color.
        return Optional.of(Move.clue(playerIndex, Hint.numberHint(card.getNumber())));
      }
    }
    return Optional.empty();
  }
  
  private boolean canHintNumber(RevealedHand hand, Community community, int cardIndex) {
    int numberToHint = hand.getCardValueAt(cardIndex).getNumber();
    for (int i = 0; i < cardIndex; i++) {
      if (hand.getCardValueAt(i).getNumber() == numberToHint) {
        // A card before the given one has the same number so the hint is not valid.
        return false;
      }
    }
    // Check that the given card has not already been hinted for this number.
    return hand.asHintedHand()
        .getHintedCardAt(cardIndex)
        .getPredicatedHints().stream()
        .noneMatch(ph -> ph.didApply() && ph.getHint().isNumberHint() 
            && ph.getHint().getHintedNumber() == numberToHint);
  }
  
  private boolean canHintColor(RevealedHand hand, Community community, int cardIndex) {
    Color colorToHint = hand.getCardValueAt(cardIndex).getColor();
    for (int i = 0; i < cardIndex; i++) {
      if (hand.getCardValueAt(i).getColor() == colorToHint) {
        // A card before the given one has the same color so the hint is not valid.
        return false;
      }
    }
    // Check that the given card has not already been hinted for this color.
    return hand.asHintedHand()
        .getHintedCardAt(cardIndex)
        .getPredicatedHints().stream()
        .noneMatch(ph -> ph.didApply() && (
            ph.getHint().isColorHint() && ph.getHint().getHintedColor() == colorToHint
            || ph.getHint().isNumberHint() && ph.getHint().getHintedNumber() == 1));
  }
  
  private Optional<Move> tryToPlay(HintedHand myHand, Community community) {
    for (int cardIndex = 0; cardIndex < myHand.size(); cardIndex++) {
      HintedCard card = myHand.getHintedCardAt(cardIndex);
      Optional<Integer> deducedNumber = deduceNumber(card);
      Optional<Color> deducedColor = deduceColor(card);
      if (deducedNumber.isPresent() && deducedColor.isPresent()) {
        CardValue cardValue = new CardValue(deducedNumber.get(), deducedColor.get());
        if (community.isPlayable(cardValue)) {
          return Optional.of(Move.play(cardIndex));
        }
      }
      if (deducedNumber.isPresent() && deducedNumber.get() == 1) {
        return Optional.of(Move.play(cardIndex));
      }
    }
    if (lastNumHints < myHand.getNumHints()) {
      // If the oldest card for which the hint applied was the oldest unclued card prior to the hint, this is a save
      // clue and the card cannot be played.
      for (int cardIndex = myHand.size() - 1; cardIndex>= 0; cardIndex--) {
        Optional<PredicatedHint> predicatedHint =
            myHand.getPredicatedHintForCardAt(myHand.getNumHints() - 1, cardIndex);
        if (!isCardHinted(myHand, cardIndex)) {
          // There is an older unclued card.
          break;
        }
        if (predicatedHint.isPresent() && predicatedHint.get().didApply()) {
          // There was no older unclued card.
          return Optional.empty();
        }
      }
      
      // Play the newest card for which the latest hint applied.
      for (int cardIndex = 0; cardIndex < myHand.size(); cardIndex++) {
        Optional<PredicatedHint> predicatedHint =
            myHand.getPredicatedHintForCardAt(myHand.getNumHints() - 1, cardIndex);
        if (predicatedHint.isPresent() && predicatedHint.get().didApply()) {
          // TODO(jven): Check that this move makes sense given either its hinted number or color.
          return Optional.of(Move.play(cardIndex));
        }
      }
    }
    return Optional.empty();
  }
  
  private Optional<Integer> deduceNumber(HintedCard card) {
    // TODO(jven): Deduce if the card is not any of the other numbers.
    return card.getPredicatedHints()
        .stream()
        .filter(predicatedHint -> predicatedHint.didApply() && predicatedHint.getHint().isNumberHint())
        .findFirst()
        .map(predicatedHint -> predicatedHint.getHint().getHintedNumber());
  }
  
  private Optional<Color> deduceColor(HintedCard card) {
    // TODO(jven): Deduce if the card is not any of the other colors.
    return card.getPredicatedHints()
        .stream()
        .filter(predicatedHint -> predicatedHint.didApply() && predicatedHint.getHint().isColorHint())
        .findFirst()
        .map(predicatedHint -> predicatedHint.getHint().getHintedColor());
  }
  
  private Optional<Move> tryToHintPlay(List<Optional<RevealedHand>> otherHands, Community community, int hints) {
    if (hints <= 0) {
      return Optional.empty();
    }
    for (int playerIndex = 0; playerIndex < otherHands.size(); playerIndex++) {
      if (!otherHands.get(playerIndex).isPresent()) {
        continue;
      }
      RevealedHand hand = otherHands.get(playerIndex).get();
      for (int cardIndex = 0; cardIndex < hand.size(); cardIndex++) {
        CardValue card = hand.getCardValueAt(cardIndex);
        if (!community.isPlayable(card)) {
          continue;
        }
        if (canHintNumber(hand, community, cardIndex)) {
          return Optional.of(Move.clue(playerIndex, Hint.numberHint(card.getNumber())));
        }
        if (canHintColor(hand, community, cardIndex)) {
          return Optional.of(Move.clue(playerIndex, Hint.colorHint(card.getColor())));
        }
      }
    }
    return Optional.empty();
  }
  
  private Optional<Move> tryToDiscard(HanabiConfig config, HintedHand myHand, int hints) {
    if (hints >= config.getMaxNumHints()) {
      return Optional.empty();
    }
    int numCards = myHand.size();
    for (int cardIndex = numCards - 1; cardIndex >= 0; cardIndex--) {
      if (!isCardHinted(myHand, cardIndex)) {
        return Optional.of(Move.discard(cardIndex));
      }
    }
    return Optional.empty();
  }
  
  private boolean isCardHinted(HintedHand hand, int cardIndex) {
    int numHints = hand.getNumHints();
    return IntStream.range(0, numHints)
        .mapToObj(hintIndex -> hand.getPredicatedHintForCardAt(hintIndex, cardIndex))
        .anyMatch(predicatedHint -> predicatedHint.isPresent() && predicatedHint.get().didApply());
  }
}
