package hanabi.conventions.examplefuncs;

import java.util.List;
import java.util.Optional;

import hanabi.common.Color;
import hanabi.common.Community;
import hanabi.common.Conventions;
import hanabi.common.Deck;
import hanabi.common.HanabiConfig;
import hanabi.common.Hint;
import hanabi.common.HintedHand;
import hanabi.common.Move;
import hanabi.common.PredicatedHint;
import hanabi.common.RevealedHand;
import hanabi.common.Trash;

/**
 * This bot is specific to the tutorial variant which deals only with red cards. The strategy is as follows:
 *
 * (1) Look for a hinted card with the value that needs to be played (again, ignoring color). If there is one, play it.
 * (2) If not, see if the other guy has one. If so and there are hints available, hint it.
 * (3) If not, discard the first card if there are few enough hints.
 * (4) Otherwise, just play the first card.  
 */
public final class ExampleFuncsConventions implements Conventions {
  
  private static final Color FAVORITE_COLOR = Color.RED;

  @Override
  public String getName() {
    return "ExampleFuncs";
  }

  @Override
  public Move makeMove(
      HanabiConfig config,
      Deck deck,
      Trash trash,
      Community community,
      List<Optional<RevealedHand>> otherHands,
      HintedHand myHand,
      int myPlayerIndex,
      int hints,
      int fuses,
      int movesAfterEmptyDeck) {
    int nextValueToPlay = community.getHighestPlayedNumberForColor(FAVORITE_COLOR) + 1;
    
    // Try to play.
    for (int i = 0; i < myHand.size(); i++) {
      for (PredicatedHint ph : myHand.getHintedCardAt(i).getPredicatedHints()) {
        if (ph.didApply() 
            && ph.getHint().isNumberHint() 
            && ph.getHint().getHintedNumber() == nextValueToPlay) {
          return Move.play(i);
        }
      }
    }
    
    // Try to hint.
    if (hints > 0) {
      for (int i = 0; i < otherHands.size(); i++) {
        if (!otherHands.get(i).isPresent()) {
          continue;
        }
        RevealedHand hand = otherHands.get(i).get();
        for (int j = 0; j < hand.size(); j++) {
          if (hand.getCardValueAt(j).getNumber() == nextValueToPlay) {
            return Move.clue(i, Hint.numberHint(nextValueToPlay));
          }
        }
      }
    }
    
    // Just discard.
    if (hints < config.getMaxNumHints()) {
      return Move.discard(0);
    }
    
    // Just play.
    return Move.play(0);
  }
}
