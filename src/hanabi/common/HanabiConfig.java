package hanabi.common;

/** Configuration values for a game of Hanabi. These values do not change between game states within the same game. */
public final class HanabiConfig {

  // The specification of the initial deck.
  private final DeckSpecification deckSpecification;
  // The total number of players in the game.
  private final int numPlayers;
  // The number of moves each player gets after the deck is emptied.
  private final int movesPerPlayerAfterEmptyDeck;
  // The number of cards in each player's hand.
  private final int initialCardsInHand;
  // The initial number of stored hints.
  private final int initialNumHints;
  // The maximum number of stored hints allowed.
  private final int maxNumHints;
  // The initial number of fuses.
  private final int initialNumFuses;
  // The maximum number of cards that can be successfully played before the game ends.
  private final int maxPlayableCards;
  
  public HanabiConfig(Builder builder) {
    deckSpecification = builder.deckSpecification;
    numPlayers = builder.numPlayers;
    movesPerPlayerAfterEmptyDeck = builder.movesPerPlayerAfterEmptyDeck;
    initialCardsInHand = builder.initialCardsInHand;
    initialNumHints = builder.initialNumHints;
    maxNumHints = builder.maxNumHints;
    initialNumFuses = builder.initialNumFuses;
    maxPlayableCards = builder.maxPlayableCards;
  }
  
  public DeckSpecification getDeckSpecification() {
    return deckSpecification;
  }
  
  public int getNumPlayers() {
    return numPlayers;
  }
  
  public int getMovesPerPlayerAfterEmptyDeck() {
    return movesPerPlayerAfterEmptyDeck;
  }
  
  public int getInitialCardsInHand() {
    return initialCardsInHand;
  }
  
  public int getInitialNumHints() {
    return initialNumHints;
  }
  
  public int getMaxNumHints() {
    return maxNumHints;
  }
  
  public int getInitialNumFuses() {
    return initialNumFuses;
  }
  
  // TODO(jven): This can be computed from the DeckSpecification. Should also validate that the spec is beatable.
  public int getMaxPlayableCards() {
    return maxPlayableCards;
  }
  
  public static Builder forDeckSpecification(DeckSpecification deckSpecification) {
    return new Builder(deckSpecification);
  }
  
  public static final class Builder {
    private final DeckSpecification deckSpecification;
    private int numPlayers = 2;
    private int movesPerPlayerAfterEmptyDeck = 1;
    private int initialCardsInHand = 5;
    private int initialNumHints = 8;
    private int maxNumHints = 8;
    private int initialNumFuses = 3;
    private int maxPlayableCards = 25;
    
    private Builder(DeckSpecification deckSpecification) {
      this.deckSpecification = deckSpecification;
    }
    
    public Builder numPlayers(int numPlayers) {
      this.numPlayers = numPlayers;
      return this;
    }
  
    public Builder movesPerPlayerAfterEmptyDeck(int movesPerPlayerAfterEmptyDeck) {
      this.movesPerPlayerAfterEmptyDeck = movesPerPlayerAfterEmptyDeck;
      return this;
    }
  
    public Builder initialCardsInHand(int initialCardsInHand) {
      this.initialCardsInHand = initialCardsInHand;
      return this;
    }
  
    public Builder initialNumHints(int initialNumHints) {
      this.initialNumHints = initialNumHints;
      return this;
    }
  
    public Builder maxNumHints(int maxNumHints) {
      this.maxNumHints = maxNumHints;
      return this;
    }
    
    public Builder initialNumFuses(int initialNumFuses) {
      this.initialNumFuses = initialNumFuses;
      return this;
    }
  
    public Builder maxPlayableCards(int maxPlayableCards) {
      this.maxPlayableCards = maxPlayableCards;
      return this;
    }
    
    public HanabiConfig build() {
      return new HanabiConfig(this);
    }
  }
}
