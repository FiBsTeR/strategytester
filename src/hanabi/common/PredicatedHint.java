package hanabi.common;

/** A hint and whether it applied to some subject (e.g. a card). */
public final class PredicatedHint {

  private final Hint hint;
  private final boolean didApply;
  
  public PredicatedHint(Hint hint, boolean didApply) {
    this.hint = hint;
    this.didApply = didApply;
  }
  
  public Hint getHint() {
    return hint;
  }
  
  public boolean didApply() {
    return didApply;
  }
}
