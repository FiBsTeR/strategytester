package hanabi.common;

/** An interface for querying the successfully played cards. */
public interface Community {
  
  /**
   * Returns the highest number of a successfully played card of the given color, or 0 if no such card has been]
   * successfully played.
   */
  public int getHighestPlayedNumberForColor(Color color);
  
  /** Returns the total number of successfully played cards. */
  public int getNumCardsPlayed();
  
  /** Returns whether the given card value can be currently played. */
  public boolean isPlayable(CardValue cardValue);
}
