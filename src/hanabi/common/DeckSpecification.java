package hanabi.common;

import java.util.Map;
import java.util.Set;

/** A specification for the contents of the initial deck. */
public interface DeckSpecification {

  /** The colors which appear at least once in the initial deck. */
  public Set<Color> getColorsUsed();
  
  /** The highest number amongst all cards in the initial deck of the given color. */
  public int getHighestNumberFor(Color color);
  
  /** The number of cards in the initial deck with the given card value. */
  public int countOf(CardValue cardValue);
  
  /** A map from each card value to its count. */
  public Map<CardValue, Integer> asMap();
}