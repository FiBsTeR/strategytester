package hanabi.common;

import java.util.List;
import java.util.Optional;

/** The user-facing interface to implement a strategy to play Hanabi. */
public interface Conventions {
  
  interface Factory {
    Conventions create();
  }

  /** The name of these conventions. */
  public String getName();
  
  /**
   * Returns the move to make given the state of the game described by the parameters.
   *
   * @param config The configuration used to initialize this game.
   * @param deck The remaining cards in the deck. The cards themselves are neither visible nor hinted.
   * @param trash The trashed cards. These cards are visible.
   * @param community The community of successfully played cards. These cards are visible.
   * @param otherHands The hands of all players except mine (the player to act), indexed by player. All optionals in the
   *     given list are present except for my player index. All cards in these hands are revealed. Hints are also
   *     available for these hands. 
   * @param myHand The hand of the player to act (me!). The cards are not visible. Instead, hints are given about the
   *     cards.
   * @param myPlayerIndex The index of the player to act (me!).
   * @param hints The number of hints remaining.
   * @param fuses The number of fuses remaining.
   * @param int The number of total moves made (across all players) since the deck was emptied.
   */
  public Move makeMove(
      HanabiConfig config,
      Deck deck,
      Trash trash,
      Community community,
      List<Optional<RevealedHand>> otherHands,
      HintedHand myHand,
      int myPlayerIndex,
      int hints,
      int fuses,
      int movesAfterEmptyDeck);
}
