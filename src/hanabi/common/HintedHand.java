package hanabi.common;

import java.util.Optional;

/** An interface for a hand with unknown card values but with hints for each card. */
public interface HintedHand {
  
  /** Returns the number of cards currently in the hand. */
  public int size();
  
  /**
   * Returns the hinted card at the given index. The 0th indexed card is the newest card in the hand and the cards are
   * sorted by age.
   */
  public HintedCard getHintedCardAt(int cardIndex);
  
  /** Returns the number of hints that have been given for this hand. */
  public int getNumHints();
  
  
  /**
   * Returns the hint for the given index. The 0th indexed hint is the chronologically first hint given for this hand
   * and the hints are sorted in descending order by age.
   */
  public Hint getHint(int hintIndex);
  
  /**
   * Returns the predicated hint for the hint and card at the given indices. The indices are interpreted the same as for
   * getHintedCardAt and getHint. If the given card was not present in the hand when the hint was given to it, an empty
   * optional is returned. Otherwise, the returned predicated hint specifies whether the hint applied to the card.
   */
  public Optional<PredicatedHint> getPredicatedHintForCardAt(int hintIndex, int cardIndex);
}
