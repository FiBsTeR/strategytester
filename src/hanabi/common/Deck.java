package hanabi.common;

/** The face-down deck of cards that have not been drawn yet. */
public interface Deck {

  /** Returns the number of cards left to be drawn from the deck. */
  public int getNumCardsRemaining();
}
