package hanabi.common;

import java.util.Comparator;

/**
 * Represents the number and color of a card. Two different cards can have the same value if they share the same number
 * and color.
 */
public final class CardValue {

  public static final Comparator<CardValue> COMPARATOR = Comparator
      .comparing(CardValue::getColor)
      .thenComparingInt(CardValue::getNumber);
  
  private final int number;
  private final Color color;
  
  public CardValue(int number, Color color) {
    this.number = number;
    this.color = color;
  }
  
  public int getNumber() {
    return number;
  }
  
  public Color getColor() {
    return color;
  }
  
  public String describe() {
    return String.format("%s%s", number, color.describe());
  }
  
  @Override
  public int hashCode() {
    return number * (Color.values().length + 1) + color.ordinal();
  }
  
  @Override
  public boolean equals(Object o) {
    if (o == null || !(o instanceof CardValue)) {
      return false;
    }
    
    CardValue otherCardValue = (CardValue) o;
    return otherCardValue.number == number && otherCardValue.color == color;
  }
}
