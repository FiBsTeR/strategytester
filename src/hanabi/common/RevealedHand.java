package hanabi.common;

/** An interface for a hand with known card values and known hints for each card. */
public interface RevealedHand {
  
  /** Returns the number of cards in the hand. */
  public int size();
  
  /**
   * Returns the card value of the card at the given index. The 0th indexed card is the newest card in the hand and the 
   * cards are sorted by age.
   */
  public CardValue getCardValueAt(int cardIndex);
  
  /** Returns the same hand with hint information. */
  public HintedHand asHintedHand();
}
