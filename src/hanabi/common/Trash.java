package hanabi.common;

import java.util.Map;

/** An interface for querying discarded and unsuccessfully played cards. */
public interface Trash {
  
  /** Returns the number of trashed cards with the given card value. */
  public int countOf(CardValue cardValue);
  
  /** Returns a map of card values to their count in the trash. */
  public Map<CardValue, Integer> asMap();
}
