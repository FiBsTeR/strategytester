package hanabi.common;

import java.util.List;

// TODO(jven): Add helper methods to help interpret the predicated hints on top of the raw list.
/** A card with unknown value but with known hints that did or did not apply to it. */
public interface HintedCard {
  
  /**
   * The hints for this card and whether or not they applied to its value. The returned list is sorted by the order the
   * hints were given.
   */
  public List<PredicatedHint> getPredicatedHints();
}
