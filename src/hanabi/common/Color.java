package hanabi.common;

/** The color (or suit) of a card. */
public enum Color {
  RED("r"),
  BLUE("b"),
  YELLOW("y"),
  GREEN("g"),
  PURPLE("p");
  
  private final String description;
  
  private Color(String description) {
    this.description = description;
  }
  
  public String describe() {
    return description;
  }
}
