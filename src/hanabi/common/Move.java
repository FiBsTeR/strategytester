package hanabi.common;

public abstract class Move {
  private Move() {}
  
  public static Move clue(int player, Hint hint) {
    return new Clue(player, hint);
  }
  
  public static Move discard(int index) {
    return new Discard(index);
  }
  
  public static Move play(int index) {
    return new Play(index);
  }
  
  public static final class Clue extends Move {
    private final int player;
    private final Hint hint;
    
    private Clue(int player, Hint hint) {
      this.player = player;
      this.hint = hint;
    }
    
    public int getPlayer() {
      return player;
    }
    
    public Hint getHint() {
      return hint;
    }
  }

  public static final class Discard extends Move {
    private final int index;
    
    private Discard(int index) {
      this.index = index;
    }
    
    public int getIndex() {
      return index;
    }
  }

  public static final class Play extends Move {
    private final int index;
    
    private Play(int index) {
      this.index = index;
    }
    
    public int getIndex() {
      return index;
    }
  }
}