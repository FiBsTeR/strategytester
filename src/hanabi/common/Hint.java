package hanabi.common;

import java.util.Optional;

public final class Hint {

  private final Optional<Integer> number;
  private final Optional<Color> color;
  
  private Hint(Optional<Integer> number, Optional<Color> color) {
    this.number = number;
    this.color = color;
  }
  
  public static Hint numberHint(int number) {
    return new Hint(Optional.of(number), Optional.empty());
  }
  
  public static Hint colorHint(Color color) {
    return new Hint(Optional.empty(), Optional.of(color));
  }
  
  public boolean isNumberHint() {
    return number.isPresent();
  }
  
  public boolean isColorHint() {
    return color.isPresent();
  }
  
  public int getHintedNumber() {
    if (!isNumberHint()) {
      throw new RuntimeException("Not a number hint.");
    }
    return number.get();
  }
  
  public Color getHintedColor() {
    if (!isColorHint()) {
      throw new RuntimeException("Not a color hint.");
    }
    return color.get();
  }
  
  public PredicatedHint applyTo(CardValue value) {
    if (number.isPresent()) {
      return new PredicatedHint(this, number.get() == value.getNumber());
    }
    if (color.isPresent()) {
      return new PredicatedHint(this, color.get() == value.getColor());
    }
    throw new RuntimeException("Invalid hint.");
  }
  
  public String describe() {
    if (number.isPresent()) {
      return "" + number.get();
    }
    if (color.isPresent()) {
      return color.get().describe();
    }
    throw new RuntimeException("Invalid hint.");
  }
}
