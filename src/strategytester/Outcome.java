package strategytester;

/**
 * The outcome of a game, eg "the first player won" for a 2-player game or "achieved 50 points" for
 * a solitaire game.
 * Two outcomes are equal if and only if they have the same value, in which case their descriptions
 * should also be the same. Games can arrive at the same outcome through different states/actions.
 */
public interface Outcome {

  public int getValue();
  
  public String describe();
}
