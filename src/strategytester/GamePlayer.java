package strategytester;

import java.util.List;

public final class GamePlayer<GAMESTATE extends GameState<GAMESTATE, ACTION>, ACTION extends Action> {
  
  public GameHistory<GAMESTATE, ACTION> play(
      GAMESTATE gameState, List<Strategy<GAMESTATE, ACTION>> strategies) {
    GameHistory.Builder<GAMESTATE, ACTION> gameHistory = GameHistory.newWithStart(gameState);
    GAMESTATE currentGameState = gameState;
    // TODO(jven): Handle possibly infinite games.
    while (!currentGameState.isTerminal()) {
      int playerToMove = currentGameState.getPlayerToAct();
      if (playerToMove < 0 || playerToMove >= strategies.size()) {
        throw new RuntimeException("Game state returned invalid player to move: " + playerToMove);
      } 
      ACTION action = strategies.get(playerToMove).act(currentGameState);
      if (!currentGameState.isActionLegal(action)) {
        return gameHistory.endWithIllegalAction(action);
      }
      gameHistory.addAction(action);
      currentGameState = currentGameState.applyAction(action);
    }
    return gameHistory.endWithOutcome();
  }
}
