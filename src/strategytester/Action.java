package strategytester;

public interface Action {
  public String describe();
}
