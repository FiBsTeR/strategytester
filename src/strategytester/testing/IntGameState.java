package strategytester.testing;

import strategytester.GameState;
import strategytester.Outcome;
import strategytester.TwoPlayerOutcome;

/**
 * A game state for tests that maintains a single state integer.
 * 
 * <p>Players alternate acting by adding numbers to the state.
 * 
 * <p>If the state reaches -1, the first player wins. If the state reaches 0, the game is drawn. If
 * the state reaches 1, the second player wins.
 */
public final class IntGameState implements GameState<IntGameState, IntAction> {

  private final int state;
  private final int playerToAct;
  
  public IntGameState(int state, int playerToAct) {
    this.state = state;
    this.playerToAct = playerToAct;
  }
  
  @Override
  public boolean isTerminal() {
    return state >= -1 && state <= 1;
  }

  @Override
  public int getPlayerToAct() {
    return playerToAct;
  }

  @Override
  public boolean isActionLegal(IntAction action) {
    return !isTerminal();
  }

  @Override
  public IntGameState applyAction(IntAction action) {
    if (isTerminal()) {
      throw new RuntimeException("Game isn't done.");
    }
    return new IntGameState(state + action.getAddToState(), 1 - playerToAct);
  }

  @Override
  public Outcome getOutcome() {
    if (state == -1) {
      return TwoPlayerOutcome.FIRST_PLAYER_WIN;
    }
    if (state == 0) {
      return TwoPlayerOutcome.DRAW;
    }
    if (state == 1) {
      return TwoPlayerOutcome.SECOND_PLAYER_WIN;
    }
    
    throw new RuntimeException("Game isn't done.");
  }
  
  @Override
  public String describe() {
    return "" + state;
  }
}
