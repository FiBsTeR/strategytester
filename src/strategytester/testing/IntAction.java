package strategytester.testing;

import strategytester.Action;

public final class IntAction implements Action {

  private final int addToState;
  
  public IntAction(int addToState) {
    this.addToState = addToState;
  }
  
  public int getAddToState() {
    return addToState;
  }
  
  @Override
  public String describe() {
    return "Adds " + addToState;
  }
}
