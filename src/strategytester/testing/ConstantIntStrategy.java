package strategytester.testing;

import strategytester.Strategy;

public final class ConstantIntStrategy implements Strategy<IntGameState, IntAction> {

  private final int addToState;
  
  public ConstantIntStrategy(int addToState) {
    this.addToState = addToState;
  }
  
  @Override
  public String getName() {
    return "Always add " + addToState;
  }

  @Override
  public IntAction act(IntGameState gameState) {
    return new IntAction(addToState);
  }

}
