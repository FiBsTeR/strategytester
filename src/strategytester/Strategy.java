package strategytester;

public interface Strategy<GAMESTATE extends GameState<GAMESTATE, ACTION>, ACTION extends Action> {
  
  public String getName();
  
  public ACTION act(GAMESTATE gameState);
}
