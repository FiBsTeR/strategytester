package strategytester;

public interface InitialGameStateProvider<GAMESTATE extends GameState<GAMESTATE, ACTION>, ACTION extends Action> {
  
  public static final class InitialGameState<GS> {
    private final long gameId;
    private final GS gameState;
    
    public InitialGameState(long gameId, GS gameState) {
      this.gameId = gameId;
      this.gameState = gameState;
    }
    
    public long getGameId() {
      return gameId;
    }
    
    public GS getGameState() {
      return gameState;
    }
  }
  
  public InitialGameState<GAMESTATE> provideRandom();
  
  public GAMESTATE provideForGameId(long gameId);
}
