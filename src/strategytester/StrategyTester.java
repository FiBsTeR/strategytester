package strategytester;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.TreeMap;

import strategytester.InitialGameStateProvider.InitialGameState;

public final class StrategyTester<
    GAMESTATE extends GameState<GAMESTATE, ACTION>, ACTION extends Action> {
  
  private final class GameHistoryWithId {
    private final long gameId;
    private final GameHistory<GAMESTATE, ACTION> history;
    
    private GameHistoryWithId(long gameId, GameHistory<GAMESTATE, ACTION> history) {
      this.gameId = gameId;
      this.history = history;
    }
    
    private long getGameId() {
      return gameId;
    }
    
    private GameHistory<GAMESTATE, ACTION> getHistory() {
      return history;
    }
  }
  
  private final class OutcomeInfo {
    private final String description;
    private final List<GameHistoryWithId> histories;
    
    private OutcomeInfo(String description) {
      this.description = description;
      histories = new ArrayList<>();
    }
    
    private void addHistory(long gameId, GameHistory<GAMESTATE, ACTION> history) {
      histories.add(new GameHistoryWithId(gameId, history));
    }
    
    private String getDescription() {
      return description;
    }
    
    private int getCount() {
      return histories.size();
    }
    
    private GameHistoryWithId getSampleHistory() {
      if (histories.size() <= 0) {
        throw new RuntimeException("There are no histories for this outcome.");
      }
      return histories.get(0);
    }
  }
  
  public final class Result {
    
    private final List<Strategy<GAMESTATE, ACTION>> strategies;
    private final Map<Integer, OutcomeInfo> outcomeInfos;
    private OutcomeInfo illegalActionInfo;
    
    public Result(List<Strategy<GAMESTATE, ACTION>> strategies) {
      this.strategies = strategies;
      outcomeInfos = new TreeMap<>();
      illegalActionInfo = new OutcomeInfo("Illegal action");
    }
    
    private void addHistory(long gameId, GameHistory<GAMESTATE, ACTION> history) {
      if (history.getOutcome().isPresent()) {
        Outcome outcome = history.getOutcome().get();
        int value = outcome.getValue();
        outcomeInfos.putIfAbsent(value, new OutcomeInfo(outcome.describe()));
        outcomeInfos.get(value).addHistory(gameId, history);
      } else if (history.getIllegalAction().isPresent()) {
        illegalActionInfo.addHistory(gameId, history);
      }
    }
    
    private int totalGames() {
      return illegalActionInfo.getCount() 
          + outcomeInfos.values().stream().mapToInt(info -> info.getCount()).sum();
    }
    
    /**
     * Prints a summary of the strategies and number of games contained in this result. Returns this
     * result for chaining.
     */
    public Result printSummary() {
      System.out.println(
          String.format(
                "===== %s vs %s (%s total games) =====\n", 
                strategies.get(0).getName(), 
                strategies.get(1).getName(), 
                totalGames()));
      return this;
    }
    
    /** Prints aggregated statistics of the outcomes. Returns this result for chaining. */
    public Result printStatistics() {
      int totalGames = totalGames();
      if (totalGames <= 0) {
        return this;
      }
      int modeOutcome = outcomeInfos.entrySet()
          .stream()
          .sorted(Comparator.<Map.Entry<Integer, OutcomeInfo>>comparingInt(
              e -> e.getValue().getCount()).reversed())
          .findFirst()
          .get()
          .getKey();
      int averageOutcome = outcomeInfos.entrySet()
          .stream()
          .mapToInt(e -> e.getKey() * e.getValue().getCount())
          .sum() / totalGames;
      
      int totalSoFar = 0;
      Optional<Integer> medianOutcome = Optional.empty();
      for (Map.Entry<Integer, OutcomeInfo> entry : outcomeInfos.entrySet()) {
        totalSoFar += entry.getValue().getCount();
        if (totalSoFar >= totalGames / 2) {
          medianOutcome = Optional.of(entry.getKey());
          break;
        }
      }
      if (!medianOutcome.isPresent()) {
        throw new RuntimeException("Couldn't calculate median outcome.");
      }
      
      System.out.println(
          String.format(
              "Average outcome: %s\nMedian outcome: %s\nModal outcome: %s\n", 
              averageOutcome,
              medianOutcome.get(),
              modeOutcome));
      return this;
    }
    
    /** Prints a histogram of the outcomes. Returns this result for chaining. */
    public Result printHistogram() {
      outcomeInfos.values()
          .forEach(outcomeInfo -> {
            System.out.println(
                String.format(
                    "%s: %s games", 
                    outcomeInfo.getDescription(), 
                    outcomeInfo.getCount()));
          });
      if (illegalActionInfo.getCount() > 0) {
        System.out.println(
            String.format(
                  "[ %s: %s games ]", 
                  illegalActionInfo.getDescription(), 
                  illegalActionInfo.getCount()));
      }
      System.out.println("");
      return this;
    }
    
    /** Prints a sample history for the given outcome value. Returns this result for chaining. */
    public Result printSampleHistoryForOutcome(int outcomeValue) {
      if (outcomeInfos.containsKey(outcomeValue)) {
        OutcomeInfo outcomeInfo = outcomeInfos.get(outcomeValue);
        System.out.println(String.format(
            "'%s' sample history (game ID = %s):\n\n%s\n", 
            outcomeInfo.getDescription(),
            outcomeInfo.getSampleHistory().getGameId(),
            outcomeInfo.getSampleHistory().getHistory().describe()));
      }
      return this;
    }
  
    /**
     * Prints a sample history for a game in which an illegal action was attempted. Returns this
     * result for chaining.
     */
    public Result printSampleHistoryForIllegalAction() {
      if (illegalActionInfo.getCount() > 0) {
        System.out.println(String.format(
            "Illegal action sample history (game ID = %s):\n\n%s\n", 
            illegalActionInfo.getDescription(),
            illegalActionInfo.getSampleHistory().getGameId(),
            illegalActionInfo.getSampleHistory().getHistory().describe()));
      }
      return this;
    }
  
    /**
     * Prints a sample history for each outcome that occurred in at least one game. Also prints a
     * sample history for a game in which an illegal action was attempted, if applicable. Returns
     * this result for chaining.
     */
    public Result printSampleHistoryForAllOutcomes() {
      for (int value : outcomeInfos.keySet()) {
        printSampleHistoryForOutcome(value);
      }
      printSampleHistoryForIllegalAction();
      return this;
    }
    
    /**
     * Prints a histogram of the outcomes and a sample history for each outcome. Returns this result
     * for chaining.
     */
    public Result printAll() {
      printSummary();
      printStatistics();
      printHistogram();
      printSampleHistoryForAllOutcomes();
      return this;
    }
  }

  private final InitialGameStateProvider<GAMESTATE, ACTION> initialGameStateProvider;
  private final GamePlayer<GAMESTATE, ACTION> gamePlayer;
  
  public StrategyTester(
      InitialGameStateProvider<GAMESTATE, ACTION> initialGameStateProvider,
      GamePlayer<GAMESTATE, ACTION> gamePlayer) {
    this.initialGameStateProvider = initialGameStateProvider;
    this.gamePlayer = gamePlayer;
  }
  
  public Result testStrategies(List<Strategy<GAMESTATE, ACTION>> strategies, int numGames) {
    Result result = new Result(strategies);
    for (int i = 0; i < numGames; i++) {
      InitialGameState<GAMESTATE> initialGameState = initialGameStateProvider.provideRandom();
      GameHistory<GAMESTATE, ACTION> history = gamePlayer.play(
          initialGameState.getGameState(), strategies);
      result.addHistory(initialGameState.getGameId(), history);
    }
    
    return result;
  }
  
  public Result testStrategiesForGameId(List<Strategy<GAMESTATE, ACTION>> strategies, long gameId) {
    Result result = new Result(strategies);
    GAMESTATE initialGameState = initialGameStateProvider.provideForGameId(gameId);
    GameHistory<GAMESTATE, ACTION> history = gamePlayer.play(initialGameState, strategies);
    result.addHistory(gameId, history);
    return result;
  }
}
