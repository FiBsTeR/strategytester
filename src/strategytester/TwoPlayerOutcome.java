package strategytester;

public enum TwoPlayerOutcome implements Outcome {

  FIRST_PLAYER_WIN(-1, "First player win"),
  SECOND_PLAYER_WIN(1, "Second player win"),
  DRAW(0, "Draw");
  
  private final int value;
  private final String description;
  
  private TwoPlayerOutcome(int value, String description) {
    this.value = value;
    this.description = description;
  }
  
  @Override
  public int getValue() {
    return value;
  }

  @Override
  public String describe() {
    return description;
  }
}
