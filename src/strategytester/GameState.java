package strategytester;

/** An immutable representation of a state of the game. */
public interface GameState<GAMESTATE extends GameState<GAMESTATE, ACTION>, ACTION extends Action> {
  
  /**
   * Returns whether this is a terminal game state. That is, no more actions can be taken and the
   * state has an outcome.
   */
  boolean isTerminal();
  
  /**
   * Returns the index of the player that must take the next action. It is an error to call this for
   * a terminal game state.
   */
  int getPlayerToAct();
  
  /** Returns whether the given action is legal for the current state. */
  boolean isActionLegal(ACTION action);
  
  /**
   * Returns the resulting game state after applying the given action. It is an error to call this
   * for an illegal action.
   */
  GAMESTATE applyAction(ACTION action);
  
  /**
   * Returns the outcome for a terminal game state. It is an error to call this for a non-terminal
   * game state.
   */
  Outcome getOutcome();
  
  /** Returns a string description of the game state. */
  String describe();
}
