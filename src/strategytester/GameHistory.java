package strategytester;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public final class GameHistory<GAMESTATE extends GameState<GAMESTATE, ACTION>, ACTION extends Action> {

  private final List<GAMESTATE> gameStates;
  private final List<ACTION> actions;
  // An empty optional indicates the game ended exceptionally.
  private final Optional<Outcome> outcome;
  // If present, the illegal action which ended the game.
  private final Optional<ACTION> illegalAction;
  
  private GameHistory(
      Builder<GAMESTATE, ACTION> builder, 
      Optional<Outcome> outcome,
      Optional<ACTION> illegalAction) {
    gameStates = builder.gameStates;
    actions = builder.actions;
    this.outcome = outcome;
    this.illegalAction = illegalAction;
  }
  
  public List<GAMESTATE> getGameStates() {
    return gameStates;
  }
  
  public List<ACTION> getActions() {
    return actions;
  }
  
  public Optional<Outcome> getOutcome() {
    return outcome;
  }
  
  public Optional<ACTION> getIllegalAction() {
    return illegalAction;
  }
  
  public String describe() {
    String s = String.format("Initial game state: \n%s\nActions:", gameStates.get(0).describe());
    for (int i = 0; i < actions.size(); i++) {
      s += String.format("\n%s\nNext game state: \n%s", actions.get(i).describe(), gameStates.get(i + 1).describe());
    }
    if (outcome.isPresent()) {
      s += String.format("\nGame ended with outcome: %s", outcome.get().describe());
    } else if (illegalAction.isPresent()) {
      s += String.format("\nGame ended with illegal action: %s", illegalAction.get().describe());
    } else {
      throw new RuntimeException("Invalid game history.");
    }
    return s;
  }
  
  public static <GS extends GameState<GS, A>, A extends Action> Builder<GS, A> newWithStart(
      GS initialGameState) {
    return new Builder<GS, A>(initialGameState);
  }
  
  public static final class Builder<GS extends GameState<GS, A>, A extends Action> {
    private final List<GS> gameStates;
    private final List<A> actions;
    private GS currentGameState;
    
    public Builder(GS initialGameState) {
      gameStates = new ArrayList<GS>();
      gameStates.add(initialGameState);
      actions = new ArrayList<A>();
      currentGameState = initialGameState;
    }
    
    public Builder<GS, A> addAction(A action) {
      if (!currentGameState.isActionLegal(action)) {
        throw new RuntimeException("Invalid action.");
      }
      currentGameState = currentGameState.applyAction(action);
      gameStates.add(currentGameState);
      actions.add(action);
      return this;
    }
    
    public GameHistory<GS, A> endWithIllegalAction(A action) {
      if (currentGameState.isActionLegal(action)) {
        throw new RuntimeException("Action is not illegal.");
      }
      return new GameHistory<GS, A>(this, Optional.empty(), Optional.of(action));
    }
    
    public GameHistory<GS, A> endWithOutcome() {
      if (!currentGameState.isTerminal()) {
        throw new RuntimeException("Game isn't over.");
      }
      return new GameHistory<GS, A>(this, Optional.of(currentGameState.getOutcome()), Optional.empty());
    }
  }
}
