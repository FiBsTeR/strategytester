package rockpaperscissors;

import java.util.Optional;

import strategytester.GameState;
import strategytester.Outcome;
import strategytester.TwoPlayerOutcome;

public final class RPSGameState implements GameState<RPSGameState, RPSAction> {

  private final Optional<Hand> firstHand;
  private final Optional<Hand> secondHand;
  
  public RPSGameState(Optional<Hand> firstHand, Optional<Hand> secondHand) {
    this.firstHand = firstHand;
    this.secondHand = secondHand;
  }
  
  @Override
  public boolean isTerminal() {
    return firstHand.isPresent() && secondHand.isPresent();
  }
  
  @Override
  public int getPlayerToAct() {
    if (!firstHand.isPresent()) {
      return 0;
    } else if (!secondHand.isPresent()) {
      return 1;
    } else {
      throw new RuntimeException("The game is over.");
    }
  }

  @Override
  public boolean isActionLegal(RPSAction action) {
    // All moves are legal in this game.
    return !isTerminal();
  }

  @Override
  public RPSGameState applyAction(RPSAction action) {
    Optional<Hand> newHand = Optional.of(action.getHand());
    if (!firstHand.isPresent()) {
      return new RPSGameState(newHand, secondHand);
    } else if (!secondHand.isPresent()) {
      return new RPSGameState(firstHand, newHand);
    } else {
      throw new RuntimeException("The game is over.");
    }
  }

  @Override
  public Outcome getOutcome() {
    if (!firstHand.isPresent() || !secondHand.isPresent()) {
      throw new RuntimeException("The game is not over.");
    }
    
    Hand h1 = firstHand.get();
    Hand h2 = secondHand.get();
    if (h1 == h2) {
      return TwoPlayerOutcome.DRAW;
    } else if (h1 == Hand.ROCK && h2 == Hand.SCISSORS
        || h1 == Hand.PAPER && h2 == Hand.ROCK
        || h1 == Hand.SCISSORS && h2 == Hand.PAPER) {
      return TwoPlayerOutcome.FIRST_PLAYER_WIN;
    } else {
      return TwoPlayerOutcome.SECOND_PLAYER_WIN;
    }
  }
  
  @Override
  public String describe() {
    return String.format(
        "First player %s\nSecond player %s", describePlayer(firstHand), describePlayer(secondHand));
  }
  
  private String describePlayer(Optional<Hand> hand) {
    return hand.isPresent()
        ? "played " + hand.get() + "."
        : "has not played yet.";
  }
}
