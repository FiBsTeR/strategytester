package rockpaperscissors;

import java.util.Optional;

import strategytester.InitialGameStateProvider;

public final class RPSInitialGameStateProvider implements InitialGameStateProvider<RPSGameState, RPSAction> {

  private static final long GAME_ID = 0;
  
  @Override
  public InitialGameState<RPSGameState> provideRandom() {
    return new InitialGameState<>(
        GAME_ID,
        new RPSGameState(Optional.empty() /* firstHand */, Optional.empty() /* secondHand */));
  }
  
  @Override
  public RPSGameState provideForGameId(long gameId) {
    return provideRandom().getGameState();
  }
}
