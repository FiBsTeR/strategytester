package rockpaperscissors;
import strategytester.Strategy;

public final class RandomHandStrategy implements Strategy<RPSGameState, RPSAction> {
  
  @Override
  public String getName() {
    return "Random hand";
  }

  @Override
  public RPSAction act(RPSGameState gameState) {
    int p = (int) (3 * Math.random());
    if (p == 0) {
      return new RPSAction(Hand.ROCK);
    } else if (p == 1) {
      return new RPSAction(Hand.PAPER);
    } else {
      return new RPSAction(Hand.SCISSORS);
    }
  }
}
