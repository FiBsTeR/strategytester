package rockpaperscissors;

import strategytester.Action;

public final class RPSAction implements Action {

  private final Hand hand;
  
  public RPSAction(Hand hand) {
    this.hand = hand;
  }
  
  public final Hand getHand() {
    return hand;
  }
  
  @Override
  public String describe() {
    return "" + hand;
  }
}
