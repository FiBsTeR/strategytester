package rockpaperscissors;

import strategytester.Strategy;

public final class ConstantHandStrategy implements Strategy<RPSGameState, RPSAction> {

  private final Hand constantHand;
  
  public ConstantHandStrategy(Hand constantHand) {
    this.constantHand = constantHand;
  }
  
  @Override
  public String getName() {
    return "Always " + constantHand;
  }

  @Override
  public RPSAction act(RPSGameState gameState) {
    return new RPSAction(constantHand); 
  }
}
