package rockpaperscissors;

import java.util.ArrayList;
import java.util.List;

import strategytester.GamePlayer;
import strategytester.Strategy;
import strategytester.StrategyTester;

public final class Main {

  public static void main(String[] args) {
    Strategy<RPSGameState, RPSAction> ALWAYS_ROCK = new ConstantHandStrategy(Hand.ROCK);
    Strategy<RPSGameState, RPSAction> ALWAYS_PAPER = new ConstantHandStrategy(Hand.PAPER);
    Strategy<RPSGameState, RPSAction> RANDOM = new RandomHandStrategy();
    
    testAndPrintResults(ALWAYS_ROCK, ALWAYS_ROCK);
    testAndPrintResults(ALWAYS_ROCK, ALWAYS_PAPER);
    testAndPrintResults(ALWAYS_ROCK, RANDOM);
  }
  
  private static void testAndPrintResults(
      Strategy<RPSGameState, RPSAction> s1, Strategy<RPSGameState, RPSAction> s2) {
    StrategyTester<RPSGameState, RPSAction> strategyTester 
        = new StrategyTester<RPSGameState, RPSAction>(
              new RPSInitialGameStateProvider(), new GamePlayer<RPSGameState, RPSAction>());
    List<Strategy<RPSGameState, RPSAction>> strategies 
        = new ArrayList<Strategy<RPSGameState, RPSAction>>();
    strategies.add(s1);
    strategies.add(s2);
    strategyTester.testStrategies(strategies, 10000 /* numGames */).printAll();
  }
}
