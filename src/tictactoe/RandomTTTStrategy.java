package tictactoe;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public final class RandomTTTStrategy implements TTTStrategy {

  @Override
  public String getName() {
    return "Random";
  }

  @Override
  public TTTAction act(TTTGameState gameState) {
    Symbol mySymbol = gameState.getSymbolToAct();
    List<Position> freePositions = Arrays.stream(Position.values())
        .filter(position -> gameState.getGrid().canPutAt(mySymbol, position))
        .collect(Collectors.toList());
    int n = freePositions.size();
    if (n == 0) {
      throw new RuntimeException("No moves! :(");
    }

    return new TTTAction(mySymbol, freePositions.get((int) (n * Math.random())));
  }
}
