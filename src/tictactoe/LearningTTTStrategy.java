package tictactoe;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public final class LearningTTTStrategy implements TTTStrategy {

  private final int trainingEpisodes;
  private final double[] stateValues;

  public LearningTTTStrategy(int trainingEpisodes) {
    this.trainingEpisodes = trainingEpisodes;
    stateValues = TTTLearner.learnStateValues(trainingEpisodes);
  }

  @Override
  public String getName() {
    return "Learning " + trainingEpisodes;
  }

  @Override
  public TTTAction act(TTTGameState gameState) {
    Symbol mySymbol = gameState.getSymbolToAct();
    List<Position> freePositions = Arrays.stream(Position.values())
        .filter(position -> gameState.getGrid().canPutAt(mySymbol, position))
        .collect(Collectors.toList());
    int n = freePositions.size();
    if (n == 0) {
      throw new RuntimeException("No moves! :(");
    }

    double bestValue = -99999;
    TTTAction bestMove = null;
    for (Position position : freePositions) {
      TTTAction move = new TTTAction(mySymbol, position);
      Grid resultingGrid = gameState.applyAction(move).getGrid();
      double value = stateValues[resultingGrid.hashToInt()];
      if (mySymbol == Symbol.O) {
        value *= -1;
      }
      if (value > bestValue) {
        bestValue = value;
        bestMove = move;
      }
    }

    return bestMove;
  }
}
