package tictactoe;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

public final class OptimalTTTStrategy implements TTTStrategy {

  private static final int[] POWERS_OF_THREE = new int[] {1, 3, 9, 27, 81, 243, 729, 2187, 6561};
  
  private final Map<Integer, Position> xActions;
  private final Map<Integer, Position> oActions;
  
  public OptimalTTTStrategy() {
    xActions = new HashMap<Integer, Position>();
    oActions = new HashMap<Integer, Position>();
    precomputeActions();
  }
  
  @Override
  public String getName() {
    return "Optimal";
  }

  @Override
  public TTTAction act(TTTGameState gameState) {
    Symbol mySymbol = gameState.getSymbolToAct();
    Symbol enemySymbol = gameState.getSymbolForPlayer(1 - gameState.getPlayerToAct());
    Grid grid = gameState.getGrid();
    if (grid.checkThreeInARow() != Symbol.EMPTY) {
      throw new RuntimeException("Game is already over.");
    }
    
    List<Position> freePositions = Arrays.stream(Position.values())
        .filter(position -> gameState.getGrid().canPutAt(mySymbol, position))
        .collect(Collectors.toList());
    if (freePositions.isEmpty()) {
      throw new RuntimeException("No moves!");
    }
    
    // If any position makes me win, play it.
    for (Position position : freePositions) {
      Grid newGrid = grid.putAt(mySymbol, position);
      if (newGrid.checkThreeInARow() == mySymbol) {
        return new TTTAction(mySymbol, position);
      }
    }
    
    // If any position makes the enemy player win, block it.
    for (Position position : freePositions) {
      Grid newGrid = grid.putAt(enemySymbol, position);
      if (newGrid.checkThreeInARow() == enemySymbol) {
        return new TTTAction(mySymbol, position);
      }
    }
    
    // Lookup the current state of the grid in the precomputed list of actions, checking for all 8
    // symmetries as well (rotations and reflections).
    Optional<Position> optimalPosition = symmetricLookup(
        mySymbol == Symbol.X ? xActions : oActions, 
        new Symbol[] {
          grid.symbolAt(Position.TOP_LEFT),
          grid.symbolAt(Position.TOP),
          grid.symbolAt(Position.TOP_RIGHT),
          grid.symbolAt(Position.LEFT),
          grid.symbolAt(Position.CENTER),
          grid.symbolAt(Position.RIGHT),
          grid.symbolAt(Position.BOTTOM_LEFT),
          grid.symbolAt(Position.BOTTOM),
          grid.symbolAt(Position.BOTTOM_RIGHT)
        });
    
    // If the current grid state is found, play the corresponding move. Otherwise, just pick any
    // free position.
    Position position = optimalPosition.isPresent()
        ? optimalPosition.get()
        : freePositions.get((int) (freePositions.size() * Math.random()));
    return new TTTAction(mySymbol, position);
  }
  
  private void precomputeActions() {
    Symbol E = Symbol.EMPTY;
    Symbol X = Symbol.X;
    Symbol O = Symbol.O;
    
    // X first move
    addXAction(E, E, E, E, E, E, E, E, E, Position.CENTER);
    
    // X second move
    addXAction(O, E, E, E, X, E, E, E, E, Position.BOTTOM_RIGHT);
    addXAction(E, O, E, E, X, E, E, E, E, Position.TOP_LEFT);
    
    // X third move
    addXAction(X, O, E, E, X, E, E, E, O, Position.BOTTOM_LEFT);
    addXAction(O, E, E, E, X, O, E, E, X, Position.BOTTOM_LEFT);
    
    // O first move
    addOAction(E, E, E, E, X, E, E, E, E, Position.TOP_LEFT);
    addOAction(X, E, E, E, E, E, E, E, E, Position.CENTER);
    addOAction(E, X, E, E, E, E, E, E, E, Position.CENTER);
    
    // O second move
    addOAction(O, E, E, E, X, E, E, E, X, Position.TOP_RIGHT);
    addOAction(X, E, E, E, O, E, E, E, X, Position.TOP);
    addOAction(X, E, E, E, O, E, E, X, E, Position.BOTTOM_LEFT);
    addOAction(E, X, E, X, O, E, E, E, E, Position.TOP_LEFT);
    addOAction(E, X, E, E, O, E, X, E, E, Position.TOP_LEFT);
  }
  
  private void addXAction(
      Symbol topLeft,
      Symbol top,
      Symbol topRight,
      Symbol left,
      Symbol center,
      Symbol right,
      Symbol bottomLeft,
      Symbol bottom,
      Symbol bottomRight,
      Position actionPosition) {
    addAction(
        new Symbol[] {topLeft, top, topRight, left, center, right, bottomLeft, bottom, bottomRight}, 
        actionPosition, 
        xActions);
  }
  
  private void addOAction(
      Symbol topLeft,
      Symbol top,
      Symbol topRight,
      Symbol left,
      Symbol center,
      Symbol right,
      Symbol bottomLeft,
      Symbol bottom,
      Symbol bottomRight,
      Position actionPosition) {
    addAction(
        new Symbol[] {topLeft, top, topRight, left, center, right, bottomLeft, bottom, bottomRight}, 
        actionPosition, 
        oActions);
  }
  
  private void addAction(
      Symbol[] symbols,
      Position actionPosition,
      Map<Integer, Position> actions) {
    actions.put(computeKey(symbols), actionPosition);
  }
  
  private int computeKey(Symbol[] symbols) {
    int key = 0;
    for (int i = 0; i < 9; i++) {
      key += POWERS_OF_THREE[i] * symbols[i].getValue();
    }
    return key;
  }
  
  private Optional<Position> symmetricLookup(Map<Integer, Position> actions, Symbol[] s) {
    int key = computeKey(s);
    if (actions.containsKey(key)) {
      return Optional.of(actions.get(key));
    }
    key = computeKey(flip(s));
    if (actions.containsKey(key)) {
      return Optional.of(antiFlip(actions.get(key)));
    }
    key = computeKey(rotate(s));
    if (actions.containsKey(key)) {
      return Optional.of(antiRotate(actions.get(key)));
    }
    key = computeKey(flip(rotate(s)));
    if (actions.containsKey(key)) {
      return Optional.of(antiRotate(antiFlip(actions.get(key))));
    }
    key = computeKey(rotate(rotate(s)));
    if (actions.containsKey(key)) {
      return Optional.of(antiRotate(antiRotate(actions.get(key))));
    }
    key = computeKey(flip(rotate(rotate(s))));
    if (actions.containsKey(key)) {
      return Optional.of(antiRotate(antiRotate(antiFlip(actions.get(key)))));
    }
    key = computeKey(rotate(rotate(rotate(s))));
    if (actions.containsKey(key)) {
      return Optional.of(antiRotate(antiRotate(antiRotate(actions.get(key)))));
    }
    key = computeKey(flip(rotate(rotate(rotate(s)))));
    if (actions.containsKey(key)) {
      return Optional.of(antiRotate(antiRotate(antiRotate(antiFlip(actions.get(key))))));
    }
    
    return Optional.empty();
  }
  
  private Symbol[] rotate(Symbol[] s) {
    return new Symbol[] {s[2], s[5], s[8], s[1], s[4], s[7], s[0], s[3], s[6]};
  }
  
  private Symbol[] flip(Symbol[] s) {
    return new Symbol[] {s[2], s[1], s[0], s[5], s[4], s[3], s[8], s[7], s[6]};
  }
  
  private Position antiRotate(Position p) {
    switch (p) {
      case TOP_LEFT:
        return Position.TOP_RIGHT;
      case TOP:
        return Position.RIGHT;
      case TOP_RIGHT:
        return Position.BOTTOM_RIGHT;
      case LEFT:
        return Position.TOP; 
      case CENTER:
        return Position.CENTER;
      case RIGHT:
        return Position.BOTTOM;
      case BOTTOM_LEFT:
        return Position.TOP_LEFT;
      case BOTTOM:
        return Position.LEFT;
      case BOTTOM_RIGHT:
        return Position.BOTTOM_LEFT;
    }
    throw new RuntimeException("Invalid position.");
  }
  
  private Position antiFlip(Position p) {
    switch (p) {
      case TOP_LEFT:
        return Position.TOP_RIGHT;
      case TOP:
        return Position.TOP;
      case TOP_RIGHT:
        return Position.TOP_LEFT;
      case LEFT:
        return Position.RIGHT; 
      case CENTER:
        return Position.CENTER;
      case RIGHT:
        return Position.LEFT;
      case BOTTOM_LEFT:
        return Position.BOTTOM_RIGHT;
      case BOTTOM:
        return Position.BOTTOM;
      case BOTTOM_RIGHT:
        return Position.BOTTOM_LEFT;
    }
    throw new RuntimeException("Invalid position.");
  }
}
