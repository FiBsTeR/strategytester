package tictactoe;

public enum Symbol {
  EMPTY(0, "_"),
  X(1, "X"),
  O(2, "O");
  
  private final int value;
  private final String description;
  
  private Symbol(int value, String description) {
    this.value = value;
    this.description = description;
  }
  
  public int getValue() {
    return value;
  }
  
  public String getDescription() {
    return description;
  }

  public static Symbol fromValue(int value) {
    return Symbol.values()[value];
  }
}
