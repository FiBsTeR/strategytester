package tictactoe;

import strategytester.InitialGameStateProvider;

public final class TTTInitialGameStateProvider implements InitialGameStateProvider<TTTGameState, TTTAction> {

  private static final long GAME_ID = 0;
  
  @Override
  public InitialGameState<TTTGameState> provideRandom() {
    return new InitialGameState<>(GAME_ID, new TTTGameState(Grid.EMPTY, 0 /* playerToAct */));
  }
  
  @Override
  public TTTGameState provideForGameId(long gameId) {
    // There is only one initial game state.
    return provideRandom().getGameState();
  }
}
