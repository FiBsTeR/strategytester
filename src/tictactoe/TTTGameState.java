package tictactoe;

import strategytester.GameState;
import strategytester.Outcome;
import strategytester.TwoPlayerOutcome;

public final class TTTGameState implements GameState<TTTGameState, TTTAction> {

  private final Grid grid;
  private final int playerToAct;
  
  public TTTGameState(Grid grid, int playerToAct) {
    this.grid = grid;
    this.playerToAct = playerToAct;
  }
  
  @Override
  public boolean isTerminal() {
    return grid.isFull() || grid.checkThreeInARow() != Symbol.EMPTY;
  }

  @Override
  public int getPlayerToAct() {
    if (isTerminal()) {
      throw new RuntimeException("Game is over.");
    }
    
    return playerToAct;
  }

  @Override
  public boolean isActionLegal(TTTAction action) {
    return !isTerminal()
        && action.getSymbol() == getSymbolToAct()
        && grid.canPutAt(action.getSymbol(), action.getPosition());
  }

  @Override
  public TTTGameState applyAction(TTTAction action) {
    if (!isActionLegal(action)) {
      throw new RuntimeException("Illegal action: " + action);
    }
    
    return new TTTGameState(grid.putAt(action.getSymbol(), action.getPosition()), 1 - playerToAct);
  }

  @Override
  public Outcome getOutcome() {
    if (!isTerminal()) {
      throw new RuntimeException("Game isn't over.");
    }
    
    Symbol threeInARow = grid.checkThreeInARow();
    if (threeInARow == getSymbolForPlayer(0)) {
      return TwoPlayerOutcome.FIRST_PLAYER_WIN;
    } else if (threeInARow == getSymbolForPlayer(1)) {
      return TwoPlayerOutcome.SECOND_PLAYER_WIN;
    } else {
      return TwoPlayerOutcome.DRAW;
    }
  }
  
  @Override
  public String describe() {
    return grid.getDescription();
  }
  
  public Grid getGrid() {
    return grid;
  }
  
  public Symbol getSymbolForPlayer(int player) {
    if (player == 0) {
      return Symbol.X;
    } else if (player == 1) {
      return Symbol.O;
    }
    
    throw new RuntimeException("Invalid player: " + player);
  }
  
  public Symbol getSymbolToAct() {
    return getSymbolForPlayer(getPlayerToAct());
  }
}
