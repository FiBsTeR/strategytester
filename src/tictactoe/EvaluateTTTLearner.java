package tictactoe;

import strategytester.GamePlayer;
import strategytester.Strategy;
import strategytester.StrategyTester;

import java.util.ArrayList;
import java.util.List;

public class EvaluateTTTLearner {

  public static void main(String[] args) {
    int[] trainingEpisodesList = new int[] {1000000};
    for (int trainingEpisodes : trainingEpisodesList) {
//      testWithTrainingEpisodes(trainingEpisodes);
      testAgainstRandom(trainingEpisodes);
//      testAgainstOptimal(trainingEpisodes);
    }
  }

  private static void testWithTrainingEpisodes(int trainingEpisodes) {
    System.out.println("Num training episodes: " + trainingEpisodes);

    TTTStrategy learningStrategy = new LearningTTTStrategy(trainingEpisodes);
    TTTStrategy optimalStrategy = new OptimalTTTStrategy();
    TTTStrategy randomStrategy = new RandomTTTStrategy();
    StrategyTester<TTTGameState, TTTAction> strategyTester
        = new StrategyTester<TTTGameState, TTTAction>(new TTTInitialGameStateProvider(), new GamePlayer<>());

    List<Strategy<TTTGameState, TTTAction>> againstOptimal = new ArrayList<Strategy<TTTGameState, TTTAction>>();
    againstOptimal.add(learningStrategy);
    againstOptimal.add(optimalStrategy);
    System.out.println("Against optimal");
    strategyTester.testStrategies(againstOptimal, 10000).printHistogram();

    List<Strategy<TTTGameState, TTTAction>> againstRandom = new ArrayList<Strategy<TTTGameState, TTTAction>>();
    againstRandom.add(learningStrategy);
    againstRandom.add(randomStrategy);
    System.out.println("Against random");
    strategyTester.testStrategies(againstRandom, 10000).printHistogram();
  }

  private static void testAgainstRandom(int trainingEpisodes) {
    System.out.println("Num training episodes: " + trainingEpisodes);

    TTTStrategy learningStrategy = new LearningTTTStrategy(trainingEpisodes);
    TTTStrategy randomStrategy = new RandomTTTStrategy();
    StrategyTester<TTTGameState, TTTAction> strategyTester
        = new StrategyTester<TTTGameState, TTTAction>(new TTTInitialGameStateProvider(), new GamePlayer<>());

    List<Strategy<TTTGameState, TTTAction>> againstRandom = new ArrayList<Strategy<TTTGameState, TTTAction>>();
    againstRandom.add(randomStrategy);
    againstRandom.add(learningStrategy);
    strategyTester.testStrategies(againstRandom, 10000).printHistogram();
  }

  private static void testAgainstOptimal(int trainingEpisodes) {
    System.out.println("Num training episodes: " + trainingEpisodes);

    TTTStrategy learningStrategy = new LearningTTTStrategy(trainingEpisodes);
    TTTStrategy optimalStrategy = new OptimalTTTStrategy();
    StrategyTester<TTTGameState, TTTAction> strategyTester
        = new StrategyTester<TTTGameState, TTTAction>(new TTTInitialGameStateProvider(), new GamePlayer<>());

    List<Strategy<TTTGameState, TTTAction>> againstOptimal = new ArrayList<Strategy<TTTGameState, TTTAction>>();
    againstOptimal.add(optimalStrategy);
    againstOptimal.add(learningStrategy);
    strategyTester.testStrategies(againstOptimal, 1).printHistogram();
  }
}
