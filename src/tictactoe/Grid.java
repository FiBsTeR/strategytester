package tictactoe;

import java.util.HashMap;
import java.util.Map;

public final class Grid {
  
  private static final Map<Position, Integer> POSITION_INDICES = new HashMap<Position, Integer>();
  static {
    POSITION_INDICES.put(Position.TOP_LEFT, 0);
    POSITION_INDICES.put(Position.TOP, 1);
    POSITION_INDICES.put(Position.TOP_RIGHT, 2);
    POSITION_INDICES.put(Position.LEFT, 3);
    POSITION_INDICES.put(Position.CENTER, 4);
    POSITION_INDICES.put(Position.RIGHT, 5);
    POSITION_INDICES.put(Position.BOTTOM_LEFT, 6);
    POSITION_INDICES.put(Position.BOTTOM, 7);
    POSITION_INDICES.put(Position.BOTTOM_RIGHT, 8);
  }
  
  private static final int[][] GROUPS = new int[][] {
    new int[] {0, 1, 2},
    new int[] {3, 4, 5},
    new int[] {6, 7, 8},
    new int[] {0, 3, 6},
    new int[] {1, 4, 7},
    new int[] {2, 5, 8},
    new int[] {0, 4, 8},
    new int[] {2, 4, 6}
  };

  private final Symbol[] symbols;
  
  public Grid(Symbol[] symbols) {
    if (symbols.length != 9) {
      throw new RuntimeException("Symbols array must have length 9.");
    }
    this.symbols = symbols;
  }
  
  public static final Grid EMPTY = new Grid(new Symbol[] {
      Symbol.EMPTY,
      Symbol.EMPTY,
      Symbol.EMPTY,
      Symbol.EMPTY,
      Symbol.EMPTY,
      Symbol.EMPTY,
      Symbol.EMPTY,
      Symbol.EMPTY,
      Symbol.EMPTY
  });
  
  public Symbol symbolAt(Position position) {
    return symbols[POSITION_INDICES.get(position)];
  }

  public boolean canPutAt(Symbol symbol, Position position) {
    return symbol != Symbol.EMPTY && symbolAt(position) == Symbol.EMPTY;
  }
  
  public Grid putAt(Symbol symbol, Position position) {
    if (!canPutAt(symbol, position)) {
      throw new RuntimeException("Attempted to put symbol illegally.");
    }
    
    Symbol[] newSymbols = new Symbol[symbols.length];
    for (int i = 0; i < symbols.length; i++) {
      newSymbols[i] = symbols[i];
    }
    newSymbols[POSITION_INDICES.get(position)] = symbol;
    return new Grid(newSymbols);
  }
  
  public boolean isFull() {
    for (int i = 0; i < symbols.length; i++) {
      if (symbols[i] == Symbol.EMPTY) {
        return false;
      }
    }
    return true;
  }
  
  /**
   * Returns a non-empty symbol for which there is a row, column, or diagonal containing three of 
   * them. Returns the empty symbol if there is no such non-empty symbol.
   */ 
  public Symbol checkThreeInARow() {
    for (int[] group : GROUPS) {
      Symbol groupSymbol = checkGroup(group);
      if (groupSymbol != Symbol.EMPTY) {
        return groupSymbol;
      }
    }
    return Symbol.EMPTY;
  }
  
  public String getDescription() {
    return String.format(
        "%s%s%s/%s%s%s/%s%s%s",
        symbols[0].getDescription(),
        symbols[1].getDescription(),
        symbols[2].getDescription(),
        symbols[3].getDescription(),
        symbols[4].getDescription(),
        symbols[5].getDescription(),
        symbols[6].getDescription(),
        symbols[7].getDescription(),
        symbols[8].getDescription());
  }

  public int hashToInt() {
    int h = 0;
    int powerOfThree = 1;
    for (int i = 0; i < 9; i++) {
      h += powerOfThree * symbols[i].getValue();
      powerOfThree *= 3;
    }
    return h;
  }

  public static Grid parseFromHash(int hash) {
    Symbol[] symbols = new Symbol[9];
    for (int i = 8; i >= 0; i--) {
      symbols[i] = Symbol.fromValue(hash % 3);
      hash /= 3;
    }
    return new Grid(symbols);
  }
  
  private Symbol checkGroup(int[] group) {
    return symbols[group[0]] == symbols[group[1]] && symbols[group[1]] == symbols[group[2]]
        ? symbols[group[0]]
        : Symbol.EMPTY;
  }
}
