package tictactoe;

import strategytester.Action;

public final class TTTAction implements Action {
  
  private final Symbol symbol;
  private final Position position;
  
  public TTTAction(Symbol symbol, Position position) {
    this.symbol = symbol;
    this.position = position;
  }

  public Symbol getSymbol() {
    return symbol;
  }
  
  public Position getPosition() {
    return position;
  }
  
  @Override
  public String describe() {
    return "" + symbol + " at " + position;
  }
}
