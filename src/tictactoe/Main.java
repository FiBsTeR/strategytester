package tictactoe;

import java.util.ArrayList;
import java.util.List;

import strategytester.GamePlayer;
import strategytester.Strategy;
import strategytester.StrategyTester;

public final class Main {

  public static void main(String[] args) {
    TTTStrategy RANDOM = new RandomTTTStrategy();
    TTTStrategy HARD = new HardAITTTStrategy();
    TTTStrategy OPTIMAL = new OptimalTTTStrategy();

    testAndPrintResults(RANDOM, RANDOM);
    testAndPrintResults(RANDOM, HARD);
    testAndPrintResults(RANDOM, OPTIMAL);
    testAndPrintResults(HARD, OPTIMAL);
  }
  
  private static void testAndPrintResults(TTTStrategy s1, TTTStrategy s2) {
    StrategyTester<TTTGameState, TTTAction> strategyTester 
        = new StrategyTester<TTTGameState, TTTAction>(
              new TTTInitialGameStateProvider(), new GamePlayer<TTTGameState, TTTAction>());
    List<Strategy<TTTGameState, TTTAction>> strategies 
        = new ArrayList<Strategy<TTTGameState, TTTAction>>();
    strategies.add(s1);
    strategies.add(s2);
    strategyTester.testStrategies(strategies, 10000 /* numGames */).printAll();
  }
}
