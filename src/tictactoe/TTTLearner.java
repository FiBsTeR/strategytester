package tictactoe;

import strategytester.Outcome;
import strategytester.TwoPlayerOutcome;
import tictactoe.*;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public final class TTTLearner {

  private static double LEARNING_RATE = 0.8;
  private static double MAX_EXPLORATION_RATE = 0.99;
  private static double MIN_EXPLORATION_RATE = 0.01;
  private static double DISCOUNT_RATE = 1.0;

  public static double[] learnStateValues(int trainingEpisodes) {
    Random random = new Random();
    double[] stateValues = new double[20000];

    for (int trainingEpisode = 0; trainingEpisode < trainingEpisodes; trainingEpisode++) {
      TTTGameState gameState = new TTTGameState(Grid.EMPTY, 0 /* playerToAct */);
      List<Integer> stateHashes = new ArrayList<>();
      stateHashes.add(Grid.EMPTY.hashToInt());
      while (!gameState.isTerminal()) {
        Position nextMove = Position.CENTER;
        double explorationRate = MAX_EXPLORATION_RATE * (trainingEpisodes - trainingEpisode - 1) / (
            trainingEpisodes - 1) + MIN_EXPLORATION_RATE * trainingEpisode / (trainingEpisodes - 1);
        if (Math.random() > explorationRate) {
          // Exploitation.
          double bestMoveValue = -99999;
          for (Position position : Position.values()) {
            if (gameState.getGrid().symbolAt(position) == Symbol.EMPTY) {
              Grid nextGrid = gameState.getGrid().putAt(gameState.getSymbolToAct(), position);
              double moveValue = stateValues[nextGrid.hashToInt()];
              if (gameState.getSymbolToAct() == Symbol.O) {
                moveValue *= -1;
              }
              if (moveValue > bestMoveValue) {
                bestMoveValue = moveValue;
                nextMove = position;
              }
            }
          }
        } else {
          // Exploration.
          List<Position> freePositions = new ArrayList<>();
          for (Position position : Position.values()) {
            if (gameState.getGrid().symbolAt(position) == Symbol.EMPTY) {
              freePositions.add(position);
            }
          }
          nextMove = freePositions.get(random.nextInt(freePositions.size()));
        }
        TTTAction action = new TTTAction(gameState.getSymbolToAct(), nextMove);
        gameState = gameState.applyAction(action);
        stateHashes.add(gameState.getGrid().hashToInt());
      }

      // Update the state values.
      Outcome outcome = gameState.getOutcome();
      if (outcome == TwoPlayerOutcome.FIRST_PLAYER_WIN) {
        stateValues[stateHashes.get(stateHashes.size() - 1)] = 100;
      } else if (outcome == TwoPlayerOutcome.SECOND_PLAYER_WIN) {
        stateValues[stateHashes.get(stateHashes.size() - 1)] = -100;
      } else {
        stateValues[stateHashes.get(stateHashes.size() - 1)] = 0;
      }
      for (int i = stateHashes.size() - 2; i >= 0; i--) {
        int stateHash = stateHashes.get(i);
        double newValue = LEARNING_RATE * DISCOUNT_RATE * stateValues[stateHashes.get(i + 1)]
            + (1 - LEARNING_RATE) * stateValues[stateHash];
        stateValues[stateHash] = newValue;
      }
    }

    return stateValues;
  }

  public static void main(String[] args) {
    Grid specificPosition = Grid.EMPTY;
    specificPosition = specificPosition.putAt(Symbol.X, Position.CENTER);
    int specificPositionHash = specificPosition.hashToInt();

    int[] trainingEpisodesList = new int[] {0, 100, 10000, 100000, 1000000};
    for (int trainingEpisodes : trainingEpisodesList) {
      System.out.println("After " + trainingEpisodes + " training episodes...");
      double[] stateValues = learnStateValues(trainingEpisodes);
      int nonZeroValueStates = 0;
      for (int i = 0; i < stateValues.length; i++) {
        if (stateValues[i] != 0) {
          nonZeroValueStates++;
        }
      }
      System.out.println("Num non-zero value states: " + nonZeroValueStates);
      System.out.println("Specific position (hash = " + specificPositionHash + ") value: " + stateValues[specificPositionHash]);
    }
  }
}