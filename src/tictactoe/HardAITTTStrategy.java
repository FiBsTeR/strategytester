package tictactoe;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public final class HardAITTTStrategy implements TTTStrategy {

  private static final Position[] POSITION_PRIORITIES = new Position[] {
    Position.CENTER,
    Position.TOP_LEFT,
    Position.TOP_RIGHT,
    Position.BOTTOM_RIGHT,
    Position.BOTTOM_LEFT,
    Position.TOP,
    Position.RIGHT,
    Position.BOTTOM,
    Position.LEFT
  };
  
  @Override
  public String getName() {
    return "Hard AI";
  }

  @Override
  public TTTAction act(TTTGameState gameState) {
    Symbol mySymbol = gameState.getSymbolToAct();
    Symbol enemySymbol = gameState.getSymbolForPlayer(1 - gameState.getPlayerToAct());
    Grid grid = gameState.getGrid();
    if (grid.checkThreeInARow() != Symbol.EMPTY) {
      throw new RuntimeException("Game is already over.");
    }
    
    List<Position> freePositions = Arrays.stream(Position.values())
        .filter(position -> gameState.getGrid().canPutAt(mySymbol, position))
        .collect(Collectors.toList());
    if (freePositions.isEmpty()) {
      throw new RuntimeException("No moves!");
    }
    
    // If any position makes me win, play it.
    for (Position position : freePositions) {
      Grid newGrid = grid.putAt(mySymbol, position);
      if (newGrid.checkThreeInARow() == mySymbol) {
        return new TTTAction(mySymbol, position);
      }
    }
    
    // If any position makes the enemy player win, block it.
    for (Position position : freePositions) {
      Grid newGrid = grid.putAt(enemySymbol, position);
      if (newGrid.checkThreeInARow() == enemySymbol) {
        return new TTTAction(mySymbol, position);
      }
    }
    
    for (Position position : POSITION_PRIORITIES) {
      if (freePositions.contains(position)) {
        return new TTTAction(mySymbol, position);
      }
    }
   
    throw new RuntimeException("No moves!");
  }
}
