package tictactoe;

import strategytester.Strategy;

/** Syntactic sugar to avoid writing out all the generics. :) */
public interface TTTStrategy extends Strategy<TTTGameState, TTTAction> {}
