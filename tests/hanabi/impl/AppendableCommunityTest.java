package hanabi.impl;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.util.HashMap;
import java.util.Map;

import org.junit.Test;

import hanabi.common.CardValue;
import hanabi.common.Color;

public final class AppendableCommunityTest {

  @Test
  public void getHighestPlayedNumberForColor() {
    Map<Color, Integer> stacks = new HashMap<>();
    stacks.put(Color.RED, 3);
    stacks.put(Color.BLUE, 5);
    AppendableCommunity community = new AppendableCommunity(stacks);
    
    assertEquals(3, community.getHighestPlayedNumberForColor(Color.RED));
    assertEquals(5, community.getHighestPlayedNumberForColor(Color.BLUE));
    assertEquals(0, community.getHighestPlayedNumberForColor(Color.YELLOW));
  }

  @Test
  public void getNumCardsPlayed() {
    Map<Color, Integer> stacks = new HashMap<>();
    stacks.put(Color.RED, 3);
    stacks.put(Color.BLUE, 5);
    AppendableCommunity community = new AppendableCommunity(stacks);
    
    assertEquals(8, community.getNumCardsPlayed());
  }

  @Test
  public void isPlayable() {
    Map<Color, Integer> stacks = new HashMap<>();
    stacks.put(Color.RED, 3);
    stacks.put(Color.BLUE, 5);
    AppendableCommunity community = new AppendableCommunity(stacks);
    
    assertTrue(community.isPlayable(new CardValue(4, Color.RED)));
    assertFalse(community.isPlayable(new CardValue(5, Color.BLUE)));
    assertTrue(community.isPlayable(new CardValue(1, Color.YELLOW)));
  }

  @Test
  public void addExistingColor() {
    Map<Color, Integer> stacks = new HashMap<>();
    stacks.put(Color.RED, 3);
    stacks.put(Color.BLUE, 5);
    AppendableCommunity community = new AppendableCommunity(stacks);
    
    AppendableCommunity newCommunity = community.add(new CardValue(4, Color.RED));
    assertEquals(3, community.getHighestPlayedNumberForColor(Color.RED));
    assertEquals(4, newCommunity.getHighestPlayedNumberForColor(Color.RED));
  }

  @Test
  public void addNewColor() {
    Map<Color, Integer> stacks = new HashMap<>();
    stacks.put(Color.RED, 3);
    stacks.put(Color.BLUE, 5);
    AppendableCommunity community = new AppendableCommunity(stacks);
    
    AppendableCommunity newCommunity = community.add(new CardValue(1, Color.YELLOW));
    assertEquals(0, community.getHighestPlayedNumberForColor(Color.YELLOW));
    assertEquals(1, newCommunity.getHighestPlayedNumberForColor(Color.YELLOW));
  }

  @Test
  public void addThrows() {
    Map<Color, Integer> stacks = new HashMap<>();
    stacks.put(Color.RED, 3);
    stacks.put(Color.BLUE, 5);
    AppendableCommunity community = new AppendableCommunity(stacks);
    
    try {
      community.add(new CardValue(3, Color.RED));
      fail();
    } catch (Exception expected) {}
  }
}
