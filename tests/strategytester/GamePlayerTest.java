package strategytester;

import static org.junit.Assert.assertEquals;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.junit.Test;

import strategytester.GamePlayer;
import strategytester.Outcome;
import strategytester.Strategy;
import strategytester.TwoPlayerOutcome;
import strategytester.testing.ConstantIntStrategy;
import strategytester.testing.IntAction;
import strategytester.testing.IntGameState;

/**
 * Tests for GamePlayer, based on the fake IntGame. Players add numbers to the initial state until
 * either -1, 0, or 1 is reached. -1 is winning for the first player, 1 is winning for the second
 * player, and 0 is a draw.
 */
public final class GamePlayerTest {

  @Test
  public void initialStateTerminal() {
    assertOutcome(
        TwoPlayerOutcome.DRAW,
        0 /* initialState */,
        new ConstantIntStrategy(1),
        new ConstantIntStrategy(1));
  }

  @Test
  public void firstPlayerWin() {
    assertOutcome(
        TwoPlayerOutcome.FIRST_PLAYER_WIN,
        -11 /* initialState */,
        new ConstantIntStrategy(3),
        new ConstantIntStrategy(4));
  }

  @Test
  public void draw() {
    assertOutcome(
        TwoPlayerOutcome.DRAW,
        7 /* initialState */,
        new ConstantIntStrategy(-3),
        new ConstantIntStrategy(-4));
  }

  @Test
  public void secondPlayerWin() {
    assertOutcome(
        TwoPlayerOutcome.SECOND_PLAYER_WIN,
        -3 /* initialState */,
        new ConstantIntStrategy(5),
        new ConstantIntStrategy(-1));
  }
  
  private void assertOutcome(
      Outcome expected, 
      int initialState,
      Strategy<IntGameState, IntAction> strategy1, 
      Strategy<IntGameState, IntAction> strategy2) {
    List<Strategy<IntGameState, IntAction>> strategies 
        = new ArrayList<Strategy<IntGameState, IntAction>>();
    strategies.add(strategy1);
    strategies.add(strategy2);
    Optional<Outcome> actual = new GamePlayer<IntGameState, IntAction>().play(
        new IntGameState(initialState, 0 /* initialPlayerToAct */), strategies).getOutcome();
    assertEquals(true, actual.isPresent());
    assertEquals(expected, actual.get());
  }
}
